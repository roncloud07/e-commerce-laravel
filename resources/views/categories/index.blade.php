@extends('layouts.admin')

@section('content')
  <h4 class="header"><strong>Kategori Produk</strong></h4><br>
  <div class="divider"></div><br>
  <a href="{{ route('categories.create') }}" class="btn btn-primary btn-sm cyan darken-2">Kategori Baru</a>
  <table class="table table-hover responsive-table display" id="data-table-simple">
    <thead>
      <tr>
        <th align="center">No</th>
        <th>Nama Kategori</th>
        <th>Parent</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($categories as $k => $category)
      <tr>
        <td align="center">{{ $k+1 }}</td>
        <td>{{ $category->title }}</td>
        <td>{{ $category->parent ? $category->parent->title : '' }}</td>
        <td align="center">
          {!! Form::model($category, ['route' => ['categories.destroy', $category], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
           <a class="btn btn-primary btn-sm green darken-2" href="{{ route('categories.edit', $category->id)}}">Edit</a>
            {!! Form::submit('delete', ['class'=>'btn btn-xs btn-danger js-submit-confirm']) !!}
          {!! Form::close()!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection
