@extends('layouts.admin')

@section('content')
  <h4 class="header"><strong>Kategori Baru</strong></h4><br>
  <div class="divider"></div><br>
    {!! Form::open(['route' => 'categories.store'])!!}
      @include('categories._form')
    {!! Form::close() !!}
@endsection
