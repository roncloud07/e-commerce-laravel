<div class="form-group {!! $errors->has('nama') ? 'has-error' : '' !!}">
  {!! Form::label('nama', 'Nama') !!}
  {!! Form::text('nama', null, ['class'=>'form-control']) !!}
  {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('harga') ? 'has-error' : '' !!}">
  {!! Form::label('harga', 'Harga') !!}
  {!! Form::text('harga', null, ['class'=>'form-control']) !!}
  {!! $errors->first('harga', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('stok') ? 'has-error' : '' !!}">
  {!! Form::label('stok', 'Stok') !!}
  {!! Form::number('stok', null, ['class'=>'form-control']) !!}
  {!! $errors->first('stok', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('diskon') ? 'has-error' : '' !!}">
  {!! Form::label('diskon', 'Diskon') !!}
  {!! Form::number('diskon', null, ['class'=>'form-control']) !!}
  {!! $errors->first('diskon', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('deskripsi') ? 'has-error' : '' !!}">
  {!! Form::label('deskripsi', 'Deskripsi') !!}
  {!! Form::textarea('deskripsi', null, ['class'=>'form-control', 'size'=>'30x10']) !!}
  {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('category_lists') ? 'has-error' : '' !!}">
  {!! Form::label('category_lists', 'Kategori') !!}
  {!! Form::select('category_lists[]', [''=>'']+App\Category::lists('title','id')->all(), null, ['class'=>'form-control js-selectize', 'multiple']) !!}

  {!! $errors->first('category_lists', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {!! $errors->has('foto') ? 'has-error' : '' !!}">
  {!! Form::label('foto', 'Foto Produk (jpeg, png)') !!}
  {!! Form::file('foto') !!}
  {!! $errors->first('foto', '<p class="help-block">:message</p>') !!}

  @if (isset($model) && $model->foto !== '')
  <div class="row">
    <div class="col-md-6">
      <p>Current foto:</p>
      <div class="thumbnail">
        <img src="{{ url('/img/' . $model->foto) }}" class="img-rounded">
      </div>
    </div>
  </div>
  @endif
</div>

{!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary']) !!}
