@extends('layouts.admin')

@section('content')
<h4 class="header"><strong>Member</strong></h4><br>
<div class="divider"></div><br>
<table class="table table-hover responsive-table display" id="data-table-simple">
  <thead>
    <tr>
      <td align="center">No</td>
      <td>Nama</td>
      <td>Email</td>
      <td></td>
    </tr>
  </thead>
  <tbody>
    @foreach($users as $k => $user)
    <tr>
      <td align="center">{{ $k+1 }}</td>
      <td>{{ $user->name }}</td>
      <td>{{ $user->email }}</td>
      <td align="center">
        {!! Form::model($user, ['route' => ['users.destroy', $user], 'method' => 'delete'] ) !!}
          {!! Form::submit('delete', ['class'=>'btn btn-xs btn-danger js-submit-confirm']) !!}
        {!! Form::close()!!}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection
