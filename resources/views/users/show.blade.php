@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped responsive-table display">
          
            <tr>
              <td><strong>Nama</strong></td>
              <td>:</td>
              <td>{{ $user->name }}</td>
            </tr>
          
            <tr>
              <td><strong>Email</strong></td>
              <td>:</td>
              <td>{{ $user->email }}</td>
            </tr>
          
        </table>
      </div>
    </div>
  </div>
@endsection
