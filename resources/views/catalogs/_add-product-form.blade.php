<h4>Tambahkan ke Keranjang</h4><br>
{!! Form::open(['url' => 'cart', 'method'=>'post', 'class'=>'form-inline']) !!}
	{!! Form::hidden('product_id', $product->id) !!}

	<div class="form-group">
		{!! Form::select('quantity', ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'], '1', ['class'=>'form-control']); !!}
	</div>
	<button class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Tambahkan</button>
{!! Form::close() !!}