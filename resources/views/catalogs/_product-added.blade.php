<script>
$(document).ready(function() {
  swal({
     title: "Sukses",
     text: "Produk {{ $product_name }} berhasil ditambahkan ke cart!",
     type: "success",
     showCancelButton: true,
     confirmButtonColor: "#63BC81",
     confirmButtonText: "Konfirmasi pesanan",
     cancelButtonText: "Lanjutkan belanja",
     html: true
  }, function(isConfirm) {
      if (isConfirm) {
          window.location = "{{ url('/checkout/login') }}";
      }
  });
});
</script>
