@extends('layouts.shop')

@section('content')

@include('catalogs._banner-search');

<div class="container">
  @if($products->count() > 0)
    @if(isset($category))
      <h2 class="head">Kategori Produk {{ $cat->title }}</h2>
    @else
      <h2 class="head">{{ $products->count() }} Produk Ditemukan</h2>
    @endif
    <div class="col-md-12 upload-ad-photos">
        @foreach ($products as $p)
          <div class="col-md-3 biseller-column" style="height:335px;margin-bottom:20px">
            <a href="{{ url('details', $p->id) }}">
              <img height="255" src="{{ $p->PhotoPath }}"/>
              <span class="price">Rp{{ number_format($p->price_now) }}</span>
            </a> 
            <div class="ad-info">
              <h5>{{ $p->name }}</h5>
            </div>
          </div>
        @endforeach
    </div>
    <div class="text-center">
      {!! $products->appends(compact('category', 'q', 'sort', 'order'))->links() !!}
    </div>
  @else
    <div class="post-ad-form text-center">
      <h2 class="head">Maaf Produk Yang Anda Cari Tidak Ditemukan.</h2>
    </div>
  @endif
</div>
@endsection
