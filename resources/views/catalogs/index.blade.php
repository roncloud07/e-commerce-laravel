@extends('layouts.shop')

@section('content')
      @include('catalogs._banner-search')
      @can('customer-access')
      <div class="trending-ads">
        <div class="container">
        <!-- slider -->

        <div class="trend-ads">
              <ul style="list-style: none">
                <li>
                  @if(isset($recommendations))
                  <h2>Rekomendasi Produk Untuk Anda</h2>
                    @foreach ($recommendations as $rc)
                      <div class="col-md-3 biseller-column">
                        <a href="{{ url('details', $rc->id) }}">
                          <img height="255" src="{{ $rc->PhotoPath }}"/>
                          <span class="price">Rp{{ number_format($rc->price_now) }}</span>
                        </a>
                        <div class="ad-info">
                          <h5>{{ $rc->name }}</h5>
                        </div>
                      </div>
                      @endforeach
                  @else
                  
                  @endif

                </li>
            </ul>
          </div>   
      </div>

      @endcan

      @if($bestSellers)
        <div class="trending-ads">
          <div class="container">
              <div class="trend-ads">
                <ul id="flexiselDemo3">
                  <li>
                    <h2>Produk Terbaru</h2>
                    @foreach ($newests as $nw)
                      <div class="col-md-3 biseller-column">
                        <a href="{{ url('details', $nw->id) }}">
                          <img height="255" src="{{ $nw->PhotoPath }}"/>
                          <span class="price">Rp{{ number_format($nw->price_now) }}</span>
                        </a> 
                        <div class="ad-info">
                          <h5>{{ $nw->name }}</h5>
                        </div>
                      </div>
                    @endforeach
                  </li>
                  <li>
                    <h2>Produk Best Seller</h2>
                    @foreach ($bestSellers as $bs)
                      <div class="col-md-3 biseller-column">
                        <a href="{{ url('details', $bs->id) }}">
                          <img height="255" src="{{ url('/img/products/'.$bs->photo) }}"/>
                          <span class="price">Rp{{ number_format($bs->price) }}</span>
                        </a> 
                        <div class="ad-info">
                          <h5>{{ $bs->name }}</h5>
                        </div>
                      </div>
                    @endforeach
                  </li>
              </ul>
            <script type="text/javascript">
               $(window).load(function() {
                $("#flexiselDemo3").flexisel({
                  visibleItems:1,
                  animationSpeed: 1000,
                  autoPlay: true,
                  autoPlaySpeed: 5000,        
                  pauseOnHover: true,
                  enableResponsiveBreakpoints: true,
                  responsiveBreakpoints: { 
                    portrait: { 
                      changePoint:480,
                      visibleItems:1
                    }, 
                    landscape: { 
                      changePoint:640,
                      visibleItems:1
                    },
                    tablet: { 
                      changePoint:768,
                      visibleItems:1
                    }
                  }
                });
                
              });
               </script>
               <script type="text/javascript" src="js/jquery.flexisel.js"></script>
          </div>
        </div>
      @else
        <div class="trending-ads">
          <div class="container">
            <div class="trend-ads">
              <h2>Produk Terbaru</h2>
              @foreach ($newests as $nw)
                <div class="col-md-3 biseller-column">
                  <a href="{{ url('details', $nw->id) }}">
                    <img height="255" src="{{ $nw->PhotoPath }}"/>
                    <span class="price">Rp{{ number_format($nw->price_now) }}</span>
                  </a>
                  <div class="ad-info">
                    <h5>{{ $nw->name }}</h5>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      @endif
      <!-- //slider --> 
      
      @include('catalogs._category-panel')
      </div>
      <div class="mobile-app">
        <div class="container">
          <div class="col-md-5 app-left">
            <a href="mobileapp.html"><img src="images/app.png" alt=""></a>
          </div>
          <div class="col-md-7 app-right">
            <h3>Resale App is the <span>Easiest</span> way for Selling and buying second-hand goods</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam auctor Sed bibendum varius euismod. Integer eget turpis sit amet lorem rutrum ullamcorper sed sed dui. vestibulum odio at elementum. Suspendisse et condimentum nibh.</p>
            <div class="app-buttons">
              <div class="app-button">
                <a href="#"><img src="images/1.png" alt=""></a>
              </div>
              <div class="app-button">
                <a href="#"><img src="images/2.png" alt=""></a>
              </div>
              <div class="clearfix"> </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
@endsection
