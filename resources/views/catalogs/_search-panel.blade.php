<div class="post-ad-form" style="border:0px;margin-left: 18%">
	{!! Form::open(['url' => 'catalogs', 'method'=>'get', 'class'=>'navbar-form navbar-left']) !!}
		  {!! Form::text('q', $q, [ 'placeholder'=>'Cari produk atau kategori yang anda inginkan', 'style'=>'width:500px']) !!}
		  {!! $errors->first('q', '<p class="help-block">:message</p>') !!}

		  {!! Form::hidden('cat', $cat) !!}

			<button class="btn btn-warning" style="margin:0px 10px;height:42px;line-height:20px"><i class="fa fa-search"></i></button>
	{!! Form::close() !!}
</div>