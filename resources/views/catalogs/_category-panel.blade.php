<!-- content-starts-here -->
<div class="content">
  <div class="categories">
    <div class="container">
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=1') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-bed"></i></div>
              <h4 class="clrchg">Furniture</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=2') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-pencil"></i></div>
              <h4 class="clrchg">Office Supplies</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=3') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-laptop"></i></div>
              <h4 class="clrchg">Technology</h4>
            </div>
          </div>
        </a>
      </div>  
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=4') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-print"></i></div>
              <h4 class="clrchg">Appliances</h4>
            </div>
          </div>
        </a>
      </div>  
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=5') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-folder-open"></i></div>
              <h4 class="clrchg">Binders and Binder Accessories
</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=6') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-book"></i></div>
              <h4 class="clrchg"> 
Bookcases</h4>
            </div>
          </div>
        </a>
      </div>  
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=7') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-wheelchair"></i></div>
              <h4 class="clrchg">Chairs & Chairmats</h4>
            </div>
          </div>
        </a>
      </div>  
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=8') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-desktop"></i></div>
              <h4 class="clrchg">Computer Peripherals</h4>
            </div>
          </div>
        </a>
      </div>  
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=9') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-tags"></i></div>
              <h4 class="clrchg"> 
Labels</h4>
            </div>
          </div>
        </a>
      </div>  
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=10') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-envelope"></i></div>
              <h4 class="clrchg">Office Furnishings</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=11') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-fax"></i></div>
              <h4 class="clrchg">Office Machines</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=12') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-newspaper-o"></i></div>
              <h4 class="clrchg">Paper</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=13') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-paint-brush"></i></div>
              <h4 class="clrchg">Pens & Art Supplies</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=14') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-ravelry"></i></div>
              <h4 class="clrchg">Rubber Bands</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=15') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-scissors"></i></div>
              <h4 class="clrchg">Scissors, Rulers and Trimmers
</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=16') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-hdd-o"></i></div>
              <h4 class="clrchg">Storage & Organization
</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=17') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-archive"></i></div>
              <h4 class="clrchg">Tables
</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-2 focus-grid">
        <a href="{{ url('/catalogs?category=18') }}">
          <div class="focus-border">
            <div class="focus-layout">
              <div class="focus-image"><i class="fa fa-phone"></i></div>
              <h4 class="clrchg">Telephones and Communication

</h4>
            </div>
          </div>
        </a>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>