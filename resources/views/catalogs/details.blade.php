@extends('layouts.shop')

@section('content')
  @include('catalogs._banner-search');

  <!--single-page-->
  <div class="single-page main-grid-border">

    <div class="container">
      <div class="product-desc">
        <div class="col-md-7 product-view">
          <h2>{{ $product->name }}</h2>
          <p> <i class="fa fa-calendar-check-o"></i>Added at {{ date('d M Y | h:i:s', strtotime($product->created_at)) }}</p>

          <img src="{{ $product->PhotoPath }}" height="300">
          <div class="product-details">
            @foreach ($product->categories as $category)
              <p>Kategori <i class="fa fa-btn fa-tags"></i> | <span class="label label-primary"> {{ $category->title }}</span></p>
            @endforeach
            <p style="text-align: justify;"><strong>Summary</strong><br>{{ $product->detail }}</p>
          </div>
        </div>
        
        <div class="col-md-5 product-details-grid">
          <div class="item-price">
            <div class="product-price">
              <p class="p-price">Harga</p>
              <h3 class="rate">Rp{{ number_format($product->price_now) }}</h3>
              <div class="clearfix"></div>
            </div>
            <div class="itemtype">
              <p class="p-price">Sebelum</p>
              <h4 style="text-decoration: line-through">Rp{{ number_format($product->price) }}</h4>
              
              <div class="clearfix"></div>
            </div>
            <div class="condition">
              <p class="p-price">Diskon</p>
              <h4>{{ $product->discount }}%</h4>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="interested text-center">
            <h4>Tambahkan ke Keranjang</h4><br>
            {!! Form::open(['url' => 'cart', 'method'=>'post', 'class'=>'form-inline']) !!}
              {!! Form::hidden('product_id', $product->id) !!}

              <div class="form-group">
                {!! Form::select('quantity', ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'], '1', ['class'=>'form-control']); !!}
              </div>
              <button class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Tambahkan</button>
            {!! Form::close() !!}
          </div>
        </div>
      <div class="clearfix"></div>

      </div>
      @can('customer-access')
        @if(count($recommendations) > 0)
          <div class="col-md-7" style="border:1px solid #eee;padding:20px;float:left;margin-top:20px">
            <h4>Pelanggan yang membeli produk ini</h4><hr>
            @foreach ($recommendations as $rc)
              <div class="col-md-3">
                <small><strong>{{ $rc->confidence }}% juga membeli</strong></small>
                <a href="{{ url('details', $rc->id) }}">
                  <img src="{{ url('/img/products/'.$rc->photo) }}" height="100"><br>
                  <strong>{{ $rc->name }}</strong>
                  <p>Rp{{ number_format($rc->price) }}</p>
                </a>
              </div>
            @endforeach
          </div>
        @endif

      @endcan
    </div>
  </div>
  
  <!--//single-page-->
@endsection
