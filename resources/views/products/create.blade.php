@extends('layouts.admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4 class="header"><strong>Produk Baru</strong></h4><br>
        <div class="divider"></div><br>
        {!! Form::open(['route' => 'products.store', 'files' => true])!!}
          @include('products._form')
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
