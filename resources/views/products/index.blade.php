@extends('layouts.admin')

@section('content')
  <h4 class="header"><strong>Produk</strong></h4><br>
  <div class="divider"></div><br>
  <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm cyan darken-2">Produk Baru</a></h3>
  <table class="table table-hover responsive-table display" id="data-table-simple">
    <thead>
      <tr>
        <td align="center">No</td>
        <td>Nama</td>
        <td>Harga</td>
        <td align="center">Stok</td>
        <td width="230" align="center">Kategori</td>
        <td width="100"></td>
      </tr>
    </thead>
    <tbody>
      @foreach($products as $k => $product)
      <tr>
        <td align="center">{{ $k+1 }}</td>
        <td>{{ $product->name }}</td>
        <td>Rp{{ number_format($product->price, 2) }}</td>
        <td align="center">{{ $product->stock }}</td>
        <td>
          @foreach ($product->categories as $category)
            <span class="label label-primary">
            <i class="fa fa-btn fa-tags"></i>
            {{ $category->title }}</span>
          @endforeach
        </td>
        <td align="center">
          {!! Form::model($product, ['route' => ['products.destroy', $product], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
            <a class="btn btn-primary btn-sm green darken-2" href="{{ route('products.edit', $product->id)}}">Edit</a>
            {!! Form::submit('delete', ['class'=>'btn btn-xs btn-danger js-submit-confirm']) !!}
          {!! Form::close()!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection
