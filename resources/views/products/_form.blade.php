<div class="row">
  <div class="input-field">
    {!! Form::text('name', null, ['class'=>'validate']) !!}
    {!! Form::label('name', 'Nama') !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
  </div>
<div class="row">
  <div class="input-field">
  {!! Form::label('price', 'Harga') !!}
  {!! Form::text('price', null, ['class'=>'form-control']) !!}
  {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="row">
  <div class="input-field">
  {!! Form::label('stok', 'Stok') !!}
  {!! Form::number('stock', null, ['class'=>'form-control']) !!}
  {!! $errors->first('stock', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="row">
  <div class="input-field">
  {!! Form::label('discount', 'Diskon') !!}
  {!! Form::number('discount', null, ['class'=>'form-control']) !!}
  {!! $errors->first('discount', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="row">
  <div class="input-field">
  {!! Form::label('weight', 'Berat (gram)') !!}
  {!! Form::number('weight', null, ['class'=>'form-control']) !!}
  {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="row">
  <div class="input-field">
  {!! Form::label('detail', 'Detail') !!}
  {!! Form::textarea('detail', null, ['class'=>'materialize-textarea']) !!}
  {!! $errors->first('detail', '<p class="help-block">:message</p>') !!}
  </div>
</div>
<div class="row">
  <div class="input-field">
  {!! Form::label('category_lists', 'Kategori') !!}
  {!! Form::select('category_lists[]', [''=>'']+App\Category::lists('title','id')->all(), null, ['class'=>'js-selectize']) !!}

  {!! $errors->first('category_lists', '<p class="help-block">:message</p>') !!}
  </div>
</div>

{!! Form::label('photo', 'Foto Produk (jpeg, png)') !!}
<div class="file-field input-field">
  <div class="btn">
    <span>Foto</span>
    {!! Form::file('photo', ["class"=>"file-path validate"]) !!}
  </div>
</div>

  @if (isset($model) && $model->photo !== '')
  <div class="row">
    <div class="col-md-6">
      <p>Current foto:</p>
      <div class="thumbnail">
        <img src="{{ url('/img/products/' . $model->photo) }}" class="img-rounded">
      </div>
    </div>
  </div>
  @endif
</div>
{!! Form::submit(isset($model) ? 'Update' : 'Save', ['class'=>'btn btn-primary btn-sm cyan darken-2', 'style'=>'margin-top:50px']) !!}
