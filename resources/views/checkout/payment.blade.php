@extends('layouts.shop')

@section('content')
<div class="container">
  @include('checkout._step')
  <div class="row">
    <h2 class="head">Pembayaran</h2>
    <div class="col-xs-8 product-view">
      <div class="panel-body">
        @include('checkout._payment-form')
      </div>
    </div>
    <div class="col-xs-4">
      @include('checkout._cart-panel')
    </div>
  </div>
</div>
@endsection
