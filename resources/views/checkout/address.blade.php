@extends('layouts.shop')

@section('content')
<div class="container">
      @include('checkout._step')
        <h2 class="head">Alamat Pengiriman</h2>
        <div class="col-md-8 product-view">
          <div class="col-xs-6">
            @if (auth()->check())
              @include('checkout._address-choose-form')
            @else
              @include('checkout._address-new-form')
            @endif
          </div>
        <div class="col-xs-4">
          @include('checkout._cart-panel')
        </div>
      </div>
</div>
@endsection
