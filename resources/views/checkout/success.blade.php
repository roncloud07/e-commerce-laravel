@extends('layouts.shop')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12">

      <div class="panel-heading">
        <h2 class="head">Berhasil!</h3>
      </div>
      <div class="product-view">
        <p>Terima kasih telah berbelanja di Feloria.com <br>
          Silahkan ikuti instruksi berikut untuk melakukan pembayaran :</p>
        <ol>
          <li>Transfer uang anda ke rekening <strong>{{ config('bank-accounts')[session('order')->bank]['bank'] }} {{ config('bank-accounts')[session('order')->bank]['number'] }}</strong> An. <strong>{{ config('bank-accounts')[session('order')->bank]['name'] }}</strong>.</li>
          <li>Total pembayaran sejumlah  sejumlah <strong>Rp{{ number_format(session('order')->total_payment)}}</strong>.</li>
          <li>Lakukan konfirmasi pembayaran ke sistem jika uang sudah di transfer dengan menyertakan nomor pesanan <strong>{{ session('order')->padded_id }}</strong> & foto bukti pembayaran.</li>
          
        </ol>
      </div><br>
      <a href="{{ url("/") }}" class="btn btn-success">Lanjutkan Belanja</a>
      
    </div>
  </div>
</div>
@endsection
