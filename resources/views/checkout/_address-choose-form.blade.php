{!! Form::open(['url' => '/checkout/address', 'method'=>'post', 'class' => 'form-horizontal']) !!}

    <div class="form-group {!! $errors->has('address_id') ? 'has-error' : '' !!}">
      
        <p>Pilih Alamat Pengiriman Anda.</p>
        @foreach (auth()->user()->addresses as $address)
        <div class="row">
          <div class="col-md-1">
            <label>
              {!! Form::radio('address_id', $address->id, null) !!}
            </label>
          </div>
          <div class="col-md-11">
            <address>
             <strong>{{ $address->name }}</strong>
             {{ $address->detail }} <br>
             {{ $address->regency->name }}, {{ $address->regency->province->name }} <br>
             <abbr title="Phone">P:</abbr>  +62 {{ $address->phone }}
            </address>
          </div>
        </div>
        @endforeach
        <div class="row">
          {!! $errors->first('address_id', '<p class="help-block">:message</p>') !!}
        </div>
      </div>

    </div>

    <div class="form-group">
        <div class="col-md-8">
            {!! Form::button('Lanjut <i class="fa fa-arrow-right"></i>', array('type' => 'submit', 'class' => 'btn btn-primary')) !!}
        </div>
    </div>

{!! Form::close() !!}
