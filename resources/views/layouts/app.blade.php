<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Feloria.com</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    {{-- <link href="{{ url(elixir('css/app.css')) }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ url('plugins/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Feloria.com
                </a>
                @include('layouts._customer-feature', 
                    ['partial_view'=>'catalogs._search-panel', 
                        'data'=>[
                                  'q' => isset($q) ? $q : null,
                                  'cat' => isset($cat) ? $cat : ''
                                ]
                    ])

                {{-- 
                @include('catalogs._search-panel', [
                  'q' => isset($q) ? $q : null,
                  'cat' => isset($cat) ? $cat : ''
                ]) --}}
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    {{-- @if(Auth::check()) --}}
                        @can('admin-access')
                            <li><a href="{{ url('/home') }}"><i class="fa fa-btn fa-home"></i>Home</a></li>
                            <li><a href="{{ route('categories.index') }}"><i class="fa fa-btn fa-tags"></i>Kategori</a></li>
                            <li><a href="{{ route('products.index') }}"><i class="fa fa-btn fa-gift"></i>Produk</a></li>
                            <li><a href="{{ route('orders.index') }}"><i class="fa fa-btn fa-shopping-cart"></i>Laporan</a></li>
                            <li><a href="{{ route('users.index') }}"><i class="fa fa-btn fa-gift"></i>Member</a></li>
                            
                        @endcan
                    {{-- @endif --}}
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">

                   @include('layouts._customer-feature', ['partial_view'=>'layouts._cart-menu-bar'])
                    
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        @include('layouts._customer-feature', 
                    ['partial_view'=>'layouts._check-order-menu-bar', 'data'=>[]])
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#"><i class="fa fa-btn fa-user"></i>Akun</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @if (session()->has('flash_notification.message'))
        <div class="container">
            <div class="alert alert-{{ session()->get('flash_notification.level') }}">
                {{ session()->get('flash_notification.message') }}
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            </div>
        </div>
    @endif

    @yield('content')
    <hr width="85%">
    <div class="col-md-8 col-md-offset-1">&copy Feloria.com | Online Shop</div>

    <!-- JavaScripts -->
    {{-- <script src="{{ url(elixir('js/all.js')) }}"></script> --}}
    <script src="{{ asset('js/all.js') }}"></script>
    @if (Session::has('flash_product_name'))
        @include('catalogs._product-added', ['product_name' => Session::get('flash_product_name')])
    @endif

    <script type="text/javascript" src="{{ url('plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('plugins/data-tables/data-tables-script.js') }}"></script>
</body>
</html>
