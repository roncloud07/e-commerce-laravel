@extends('layouts.admin')

@section('content')

<div id="card-stats">
    <div class="row">
        <div class="col s12 m6 l12">
            <div class="card">
                <div class="card-content  blue darken1 white-text">
                    <h6>Welcome {{ Auth::User()->name }} <i class="mdi-action-account-circle"></i></h6>
                    <h4><i class="mdi-action-dashboard"></i> DASHBOARD <i class="mdi-action-dashboard"></i></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 l6">
            <div class="card">
                <a href="{{ route('categories.index') }}">
                    <div class="card-content  green white-text">
                        <p class="card-stats-title"><i class="mdi-action-loyalty"></i> Kategori</p>
                        <h4 class="card-stats-number">{{ $count->cc() }}</h4>
                        </p>
                    </div>
                </a>
                <div class="card-action  green darken-2">
                    <div id="clients-bar" class="center-align"></div>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l6">
            <div class="card">
                <a href="{{ route('products.index') }}">
                    <div class="card-content pink lighten-1 white-text">
                        <p class="card-stats-title"><i class="mdi-action-shopping-cart"></i> Produk</p>
                        <h4 class="card-stats-number">{{ $count->pc() }}</h4>
                        </p>
                    </div>
                </a>
                <div class="card-action  pink darken-2">
                    <div id="invoice-line" class="center-align"></div>
                </div>
            </div>
        </div>
        <a href="{{ route('orders.index') }}">
            <div class="col s12 m6 l6">
                <div class="card">
                    <div class="card-content blue-grey white-text">
                        <p class="card-stats-title"><i class="mdi-action-list"></i> Laporan</p>
                        <h4 class="card-stats-number">{{ $count->oc() }}</h4>
                        </p>
                    </div>
                    <div class="card-action blue-grey darken-2">
                        <div id="profit-tristate" class="center-align"></div>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('users.index') }}">
            <div class="col s12 m6 l6">
                <div class="card">
                    <div class="card-content purple white-text">
                        <p class="card-stats-title"><i class="mdi-social-group"></i> Member</p>
                        <h4 class="card-stats-number">{{ $count->uc() }}</h4>
                        </p>
                    </div>
                    <div class="card-action purple darken-2">
                        <div id="sales-compositebar" class="center-align"></div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
@endsection
