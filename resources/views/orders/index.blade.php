@extends('layouts.admin')

@section('content')
  <h4 class="header"><strong>Laporan Pembelian</strong></h4><br>
  <div class="divider"></div><br>
  <table class="table table-hover responsive-table display" id="data-table-simple">
    <thead>
      <tr>
        <th>No</th>
        <th>No.Order</th>
        <th>Member</th>
        <th>Status</th>
        <th>Pembayaran</th>
        <th>Tanggal Pemesanan</th>
        <th>Update terakhir</th>
        <th>Bukti Pembayaran</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @forelse($orders as $k =>  $order)
      <tr>
        <td>{{ $k+1 }}</td>
        <td>{{ $order->padded_id }}</td>
        <td>{{ $order->user->name }}</td>
        <td>{{ $order->human_status }}</td>
        <td>
          Total: <strong>{{ number_format($order->total_payment) }} </strong><br>
          Transfer ke : {{ config('bank-accounts')[$order->bank]['bank'] }} <br>
          Dari : {{ $order->sender }}
        </td>
        <td>{{ $order->created_at }}</td>
        <td>{{ $order->updated_at }}</td>
        <td>
          @if (isset($order) && $order->confirmation !== '')
            Ada
          @else
            Belum ada
          @endif
        </td>
        <td><a href="{{ route('orders.edit', $order->id)}}">Lihat Detail</a></td>
      </tr>
      @empty
        <tr>
          <td colspan="4">Tidak ada order yang ditemukan</td>
        </tr>
      @endforelse
    </tbody>
  </table>
@endsection
