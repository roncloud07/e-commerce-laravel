@extends('layouts.shop')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-hover responsive-table display" id="data-table-simple">
          <thead>
            <tr>
              <th>No. Order</th>
              <th>Tanggal Order</th>
              <th>Total</th>
              <th class="text-center" width="50">Konfirmasi</th>
              <th class="text-center">Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @forelse($orders as $order)
            <tr>
              <td>{{ $order->padded_id }}</td>
              <td>{{ $order->created_at }}</td>
              <td>Rp{{ number_format($order->total_payment, 2) }}</td>
              {!! Form::model($order, ['route' => ['customers.update', $order], 'method' =>'patch', 'files' => true, 'class'=>'form-inline'])!!}
              <td>
                @if (isset($order) && $order->confirmation !== '')
                  <div class="thumbnail">
                    <img width="200" src="{{ url('/img/confirmation/' . $order->confirmation) }}" class="img-rounded">
                  </div>
                @else
                  <h4 class="text-center">Upload bukti pembayaran anda di sini</h4><br>
                @endif
                <p>{!! Form::file('confirmation') !!}</p>
                {!! Form::submit('Upload', ['class'=>'btn btn-primary btn-block']) !!}
                {!! $errors->first('confirmation', '<p class="help-block">:message</p>') !!}
              </td>
              {!! Form::close() !!}
              <td class="text-center"><strong>{{ $order->human_status }}</strong></td>
              <td><a class="btn btn-info" href="{{ route('customers.show', $order->id) }}">Lihat Detail</a></td>
            </tr>
            @empty
              <tr>
                <td colspan="4">Tidak ada order yang ditemukan</td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
