@extends('layouts.shop')

@section('content')
  <div class="container">
    <div class="row" style="margin-top: 50px;font-size: 12pt">
      <div class="col-md-12">
          @forelse($orders as $order)
          <h3>Detail Order {{ $order->padded_id }}</h3><br>
          <table class="table table-bordered table-condensed">
            <thead>
              <tr>
                <th>Tanggal Order</th>
                <th>Pembayaran</th>
                <th>Detail</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{ $order->created_at }}</td>
                <td>
                <table>
                  <tr>
                    <td>Bank</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>{{ config('bank-accounts')[$order->bank]['bank'] }}</td>
                  </tr>
                  <tr>
                    <td>No.Rekening</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>{{ config('bank-accounts')[$order->bank]['number'] }}</td>
                  </tr>
                  <tr>
                    <td>Atas Nama</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>{{ config('bank-accounts')[$order->bank]['name'] }}</td>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>{{ number_format($order->total_payment) }}</td>
                  </tr>
                  <tr>
                    <td>Nama Pemesan</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td>{{ $order->sender }}</td>
                  </tr>
                </table>
                  
                </td>
                <td>
                  @include('orders._details', compact('order'))
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="4">Tidak ada order yang ditemukan</td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <a href="{{ url('/customers') }}">Kembali</a>
    </div>
  </div>
@endsection
