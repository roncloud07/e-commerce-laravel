@extends('layouts.shop')

@section('content')
<div id="page-wrapper" class="sign-in-wrapper">
    <div class="graphs">
        <div class="sign-in-form">
            <div class="sign-in-form-top">
                <h1>Log in</h1>
            </div>
            <div class="signin">
                
                <form role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <div class="log-input">
                    <div class="log-input-left">
                       <input type="text" class="user" name="email" value="{{ old('email') }}" placeholder="Masukan E-mail Anda" />
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="log-input">
                    <div class="log-input-left">
                       <input type="password" class="lock" name="password"placeholder="Password" />
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="signin-rit">
                    <span class="checkbox1">
                        <label class="checkbox">
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </span>
                    <div class="clearfix"> </div>
                </div>
                <input type="submit" class="btn btn-primary" value="Login"><br>
                <div class="sub_home_right">
                    <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                </div>
                <br>
            </form>  
            </div>
            
        </div>
    </div>
</div>
@endsection
