<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    public $timestamps = false;

    protected $fillable = ['product_id', 'recommendation_id', 'support', 'confidence'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
