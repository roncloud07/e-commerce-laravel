<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Recommendation;
use App\Order;
use App\OrderDetail;
use App\Http\Controllers\CheckoutController;

class CatalogsController extends Controller
{
    public function index(Request $request)
    {
        $newests = Product::orderBy('id', 'desc')->paginate(4);
        $bestSellers = Product::bestSellers();

        Auth::user() ? $user_id = Auth::user()->id : $user_id = "";
        // if(Auth::user() && Order::where(['user_id'=>$user_id])->count() > 0){
        //     $od_id           = Order::where(['user_id'=>$user_id])->lists('id')[0];//<--
        //     $pr_id           = OrderDetail::where(['order_id'=>$od_id])->lists('product_id');
        //     $recommendations = Product::whereIn('id', $pr_id)->take(5)->get();
        // }
        if(Auth::check() && Auth::user()->role != "admin"){
            $user_id         = Auth::user()->id;
            // mencari data transaksi berdasarkan user_id
            $od_id = Order::where(['user_id'=>$user_id])->pluck('id');
            if($od_id->count() != 0){
                // mencari data detail transaksi dengan order_id
                $odd_id = OrderDetail::where('order_id', $od_id[0])->lists('product_id');
                // jika data transaksinya sendiri jangan direkomendasikan
                // mencari rekomendasi produk dengan data order
                $rc_id = Recommendation::whereIn('product_id', $odd_id)->where("user_id", "!=", Auth::user()->id)->lists('recommendation_id');
                if($rc_id->count() != 0){
                    // mencari data produk rekomendasi
                    $recommendations = Product::whereIn('id', $rc_id)->inRandomOrder()->take(4)->get();
                }
            }
        }
        // if( Auth::check() && 
        //     Auth::user()->role != "admin" && 
        //     Recommendation::get()->count() != 0 && 
        //     $od_id != null)
        //     {
        //         // join dengan tabel recommendation
        //         $rc_id = Recommendation::whereIn('product_id', $pr_id)->lists('recommendation_id');
        //         $recommendations = Product::whereIn('product_id', $rc_id)->inRandomOrder()->take(4)->get();
        //     }

        return view('catalogs.index', compact(
                                        'newests',
                                        'bestSellers',
                                        'recommendations'
                                      ));
    }

    public function search(Request $request)
    {
        $q = $request->get('q');
            $products = Product::where('name', 'LIKE', '%'.$q.'%')->paginate(8);

        return view('catalogs.products', compact('products'));
    }

    public function r(Request $request)
    {
        CheckoutController::recommendation_process();
    }

    public function catalogs(Request $request)
    {
        if ($request->has('category')) {
            $category = $request->get('category');
            $cat = Category::findOrFail($category);
            // we use this to get product from current category and its child
            $products = Product::whereIn('id', $cat->related_products_id)->paginate(12);
        }

        if ($request->has('sort')) {
            $sort = $request->get('sort');
            $order = $request->has('order') ? $request->get('order') : 'asc';
            $field = in_array($sort, ['price', 'name']) ? $request->get('sort') : 'price';
            $products = $products->orderBy($field, $order);
        }
        
        // $idr = Recommendation::get()->lists('recommendation_id');
        // ambil data produk sesuai dengan id rekomendasi yang terpilih
        // $recommendations = Product::whereIn('id', $idr)->take(4)->get();

        return view('catalogs.products', compact(
                                        'category', 
                                        'cat', 
                                        'products', 
                                        'sort', 
                                        'order'
                                      ));
    }

    public function details($id)
    {
        $product = Product::find($id);
        // echo $product->price_now;
        if(Auth::user()){
            
            $recommendations = \DB::table('recommendations')
                ->join('products', 'recommendations.recommendation_id', '=', 'products.id')
                ->whereRaw("1 = 1")
                ->where('recommendations.product_id', $id)
                ->select('recommendations.confidence', 'products.*')
                ->groupBy('recommendation_id')
                ->inRandomOrder()
                ->take(4)
                ->get();
            // cari id rekomendasi sesuai dengan id produk yg dipilih
            // $idr = Product::find($id)->recommendations->lists('recommendation_id');
            // ambil data produk sesuai dengan id rekomendasi yang terpilih
            // $recommendations = Product::whereIn('id', $idr)->take(4)->get();

                // var_dump($t);
        }

        return view('catalogs.details', compact('product', 'recommendations'));
    }

    public function reviews()
    {
        return $reviews = \DB::select(\DB::raw(
                            "SELECT products.* FROM reviews JOIN products 
                            WHERE reviews.product_id = products.id 
                            GROUP BY(reviews.product_id)  
                            ORDER BY sum(rating) DESC LIMIT 4"
                        ));
    }



}
