<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutLoginRequest;
use App\Http\Requests\CheckoutAddressRequest;
use Auth;
use Illuminate\Support\MessageBag;
use App\User;
use App\Address;
use App\Product;
use App\Support\CartService;
use App\Order;
use App\OrderDetail;
use App\Recommendation;

class CheckoutController extends Controller
{
    protected $cart;

    public function __construct(CartService $cart)
    {
        $this->cart = $cart;

        $this->middleware('checkout.have-cart', [
            'only' => ['login', 'postLogin',
                'address', 'postAddress',
                'payment', 'postPayment']
        ]);

        $this->middleware('checkout.login-step-done', [
            'only' => ['address', 'postPayment',
                'payment', 'postPayment']
        ]);

        $this->middleware('checkout.address-step-done', [
            'only' => ['payment', 'postPayment']
        ]);

        $this->middleware('checkout.payment-step-done', [
            'only' => ['success']
        ]);

    }

    public function login()
    {
        if (Auth::check()) {
            return redirect('/checkout/address');
        } else {
            return view('checkout.login');
        }
    }

    public function postLogin(CheckoutLoginRequest $request)
    {
        $email = $request->get('email');
        $password = $request->get('checkout_password');
        $is_guest = $request->get('is_guest') > 0;

        if ($is_guest) {
            return $this->guestCheckout($email);
        }

        return $this->authenticatedCheckout($email, $password);
    }

    protected function guestCheckout($email)
    {
        // check if user exist, if so, ask login
        if ($user = User::where('email', $email)->first()) {
            if ($user->hasPassword()) {
                $errors = new MessageBag();
                $errors->add('checkout_password', 'Alamat email "' . $email . '" sudah terdaftar, silahkan masukan password.');
                // redirect and change is_guest value
                return redirect('checkout/login')->withErrors($errors)
                    ->withInput(compact('email') + ['is_guest' => 0]);
            }
            // show view to request new password
            session()->flash('email', $email);
            return view('checkout.reset-password');
        }
        // save user data to session
        session(['checkout.email' => $email]);
        return redirect('checkout/address');
    }

    protected function authenticatedCheckout($email, $password)
    {
        // login
        if (!Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication failed..
            $errors = new MessageBag();
            $errors->add('email', 'Data user yang dimasukan salah');
            return redirect('checkout/login')
                ->withInput(compact('email', 'password') + ['is_guest' => 0])
                ->withErrors($errors);
        }

        // logged in, merge cart (destroy cart cookie)
        $deleteCartCookie = $this->cart->merge();
        return redirect('checkout/address')->withCookie($deleteCartCookie);
    }

    public function address()
    {
        return view('checkout.address');
    }

    public function postAddress(CheckoutAddressRequest $request)
    {
        if (Auth::check()) return $this->authenticatedAddress($request);
        return $this->guestAddress($request);
    }

    protected function authenticatedAddress(CheckoutAddressRequest $request)
    {
        $address_id = $request->get('address_id');
        // clear old
        session()->forget('checkout.address');
        if ($address_id == 'new-address') {
            $this->saveAddressSession($request);
        } else {
            session(['checkout.address.address_id' => $address_id]);
        }
        return redirect('checkout/payment');
    }

    protected function guestAddress(CheckoutAddressRequest $request)
    {
        $this->saveAddressSession($request);
        return redirect('checkout/payment');
    }

    protected function saveAddressSession(CheckoutAddressRequest $request)
    {
        session([
            'checkout.address.nama'        => $request->get('nama'),
            'checkout.address.detail'      => $request->get('detail'),
            'checkout.address.province_id' => $request->get('province_id'),
            'checkout.address.regency_id'  => $request->get('regency_id'),
            'checkout.address.phone'       => $request->get('phone')
        ]);
    }

    public function payment()
    {
        return view('checkout.payment');
    }

    public function postPayment(Request $request)
    {
        $this->validate($request, [
            'bank_name' => 'required|in:' . implode(',',array_keys(config('bank-accounts'))),
            'sender' => 'required'
        ]);

        session([
            'checkout.payment.bank' => $request->get('bank_name'),
            'checkout.payment.sender' => $request->get('sender')
        ]);

        if (Auth::check()) return $this->authenticatedPayment($request);
        return $this->guestPayment($request);
    }

    protected function authenticatedPayment(Request $request)
    {
        $user = Auth::user();
        $bank = session('checkout.payment.bank');
        $sender = session('checkout.payment.sender');
        $address = $this->setupAddress($user, session('checkout.address'));
        $order = $this->makeOrder($user->id, $bank, $sender, $address, $this->cart->details());
        // delete session data
        session()->forget('checkout');
        $this->cart->clearCartRecord();
        // jalankan proses pembuatan rekomendasi
        $this->recommendation_process();
        return redirect('checkout/success')->with(compact('order'));
    }

    protected function guestPayment(Request $request)
    {
        // create user account
        $user = $this->setupCustomer(session('checkout.email'), session('checkout.address.name'));

        // create address
        $bank = session('checkout.payment.bank');
        $sender = session('checkout.payment.sender');
        $address = $this->setupAddress($user, session('checkout.address'));

        // create record
        $order = $this->makeOrder($user->id, $bank, $sender, $address, $this->cart->details());

        // delete session data
        session()->forget('checkout');
        $deleteCartCookie = $this->cart->clearCartCookie();
        return redirect('checkout/success')->with(compact('order'))
            ->withCookie($deleteCartCookie);
    }

    protected function setupCustomer($email, $name)
    {
        $user = User::create(compact('email', 'name'));
        $user->role = 'customer';
        $user->save();
        return $user;
    }

    protected function setupAddress(User $customer, $addressSession)
    {
        if (Auth::check() && isset($addressSession['address_id'])) {
            return Address::find($addressSession['address_id']);
        }

        return Address::create([
            'user_id' => $customer->id,
            'name' => $addressSession['name'],
            'detail' => $addressSession['detail'],
            'regency_id' => $addressSession['regency_id'],
            'phone' => $addressSession['phone']
        ]);
    }

    // Fungsi untuk pembelian barang
    protected function makeOrder($user_id, $bank, $sender, Address $address, $cart)
    {
        $status = 'waiting-payment';
        $address_id = $address->id;
        $order = Order::create(compact('user_id', 'address_id', 'bank', 'sender', 'status'));
        foreach ($cart as $product) {
            OrderDetail::create([
                'order_id' => $order->id,
                'address_id' => $address->id,
                'product_id' => $product['id'],
                'quantity' => $product['quantity'],
                'price' => $product['detail']['price'],
                'fee' => Product::find($product['id'])->getCostTo($address->regency_id)
            ]);
        }

        return Order::find($order->id);
    }

    public function success()
    {
        return view('checkout.success');
    }

    public static function recommendation_process()
    {
        // Recommendation::truncate();
        // $user_id = Auth::user()->id;
        // $od_id = Order::where(['user_id'=>$user_id])->lists('id')[0];
        // $pr_id = OrderDetail::where(['order_id'=>$od_id])->lists('product_id');
        // $products = Product::whereIn('id', $pr_id)->get();
        $products = Product::get();

        foreach ($products as $p) {
            $item[] = $p->name;
        }
        $item1 = count($item) - 1; // minus 1 from count
        $item2 = count($item);
        // query untuk membuat tabel transaksi pembelian
        $transactions = \DB::select(\DB::raw(
                            "select group_concat(products.name separator ',') as 'transaksi'
                            from order_details left join products 
                            on (order_details.product_id = products.id)
                            group by order_details.order_id"
                        ));
            
        foreach ($transactions as $t) {
            $transaksi[] = $t->transaksi;
        }
        
        // print_r($transaksi);
        $D = count($transaksi);
        $minSup = 0.02;
        $minConf = 0.3;

        $freq_item1 = array();
        $freq_item2 = array();
        $item_array = array();
        
        // --> 1 ITEMSET
        // PROSES UNTUK MENAMPILKAN JUMLAH KEMUNCULAN ITEM DALAM TRANSAKSI

        foreach ($item as $it) {
            $total_item[$it] = 0;

            foreach($transaksi as $tr) {
                if(strpos($tr, $it) !== false)
                {
                    $total_item[$it]++;
                }

            // echo $tr->transaksi;
            }

            $total_item[$it] /= $D;
            if($total_item[$it] >= $minSup)
            {
                $freq_item1[$it] = $total_item[$it];
            }
        }

        // echo "1 ITEMSET";
        // echo "<pre>";
        // print_r($freq_item1);
        // echo "</pre>";

        // $total_fi1 = count($freq_item1);
        // $total_fi2 = $total_fi1 - 1;

        for($i = 0; $i < $item2; $i++) {
            for($j = $i+1; $j < $item1; $j++) {
                $item_comb[$item[$i].' | '.$item[$j]] = 0;
                
                foreach($transaksi as $tr) {
                    if((strpos($tr, $item[$i]) !== false) && (strpos($tr, $item[$j]) !== false)) {
                        $item_comb[$item[$i].' | '.$item[$j]]++;
                    }
                }

                $item_comb[$item[$i].' | '.$item[$j]] /= $D;
                if($item_comb[$item[$i].' | '.$item[$j]] >= $minSup){
                    $freq_item2[$item[$i].' | '.$item[$j]] = $item_comb[$item[$i].' | '.$item[$j]];
                }
            }
        }
        // echo "2 ITEMSET";
        // echo "<pre>";
        // print_r($item_comb);
        // echo "</pre>";
        // echo "Step 3: Jumlah Gabungan Item<br>";
        // $no=1;
        foreach ($freq_item2 as $fi2 => $v2) {
            foreach ($freq_item1 as $fi1 => $v1) {
                // untuk membatasi agar item fi1 tidak tampil semua 
                // jadi yang ditampilkan hanya item fi1 yang sesuai dengan item kombinasi di fi2
                // cth : A. gula-minyak, 1. gula 2. minyak ...
                if ((strpos($fi2, $fi1) !== false)) {

                    $conf = $v2 / $v1;
                    list($itemx, $itemy) = explode(' | ', $fi2);
                    // echo $itemx." ".$itemy."<br>";
                    // untuk menentukan item yang berikutnya muncul
                    // cth : if $itemx(gula) == $fi1(gula) then $theitem = $itemy(minyak)
                    if ($itemx == $fi1) {
                        $theitem = $itemy;
                    } else { 
                        $theitem = $itemx; 
                    }
                    if($conf >= $minConf){
                        $p = Product::where('name', 'LIKE', '%'.$fi1.'%')->pluck('id')->all()[0];
                        $r = Product::where('name', 'LIKE', '%'.$theitem.'%')->pluck('id')->all()[0];
                        $s = round($v2 * 100, 2);
                        $c = round($conf * 100, 2);
                        $rcm = [
                                    'product_id'=>$p, 
                                    'recommendation_id'=>$r, 
                                    'support'=>$s, 
                                    'confidence'=>$c, 
                                    'user_id'=>Auth::user()->id
                                ];

                        // jika recommendation_id sudah ada di database maka update id tersebut
                        // if( Recommendation::where('recommendation_id', 70)->get()){
                        //     Recommendation::create($rcm);
                        // }else{
                        //     Recommendation::update($rcm);
                        // }

                        Recommendation::create($rcm);
                        
                        // echo "<strong>[$no]</strong> $fi1 <strong>--</strong> $theitem($v2) --> $fi1($v1) => ".  $conf ."<br>";

                        // echo "<strong>".$c."%</strong> yang membeli <strong>$fi1</strong> juga membeli <strong>$theitem</strong> <p>";
                        // $no++;
                    }
                }
            }
        }
    }
}
