<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Mail;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::member()->get();
        return view('users.index', compact('users'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return User::find($id)->delete();
        \Flash::success('Member berhasil dihapus.');
        return redirect()->route('users.index');
    }

    public function mail(Request $request)
    {
        // $data = new \stdClass();
        // $data->nama_lengkap = "Ronny Fernando";
        // $data->tgl_lahir = "17-08-1945";
        // $data->username = "brillian";
        // Mail::send('email', ['data' => $data], function ($m) use ($data) {
        //             $m->from('brillianmusyafa@gmail.com','brillian');
        //             $m->to('brillianmusyafa@gmail.com','brillian')->subject('Your Reminder!');
        // });

        // Variable data ini yang berupa array ini akan bisa diakses di dalam "view".
        $data = ['prize' => 'Piring Cantik', 'total' => 3 ];
     
        // "emails" adalah nama view.
        Mail::send('email', $data, function ($mail)
        {
          // Email dikirimkan ke address "momo@deviluke.com" 
          // dengan nama penerima "Momo Velia Deviluke"
          $mail->to('momo@deviluke', 'Momo Velia Deviluke');
     
          // Copy carbon dikirimkan ke address "haruna@sairenji" 
          // dengan nama penerima "Haruna Sairenji"
          $mail->cc('haruna@sairenji', 'Haruna Sairenji');
     
          $mail->subject('Hello World!');
        });
    }
}
