<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Support\CartService;
use App\Order;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $orders = Order::orderBy('updated_at', 'desc')->get();

        return view('orders.index', compact('orders'));
    }

    public function edit($id)
    {
        $order = Order::find($id);
        return view('orders.edit')->with(compact('order'));
    }

    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $this->validate($request, [
            'status' => 'required|in:' . implode(',',Order::allowedStatus())
        ]);
        $order->update($request->only('status'));
        \Flash::success($order->padded_id . ' berhasil disimpan.');
        return redirect()->route('orders.index');
    }

}
