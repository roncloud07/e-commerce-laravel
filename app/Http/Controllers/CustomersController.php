<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class CustomersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:customer');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = auth()->user()->orders()->get();

        return view('customer.view-orders')->with(compact('orders'));
        // return  auth()->user()->orders()->findOrFail($id);
    }

    public function show($id)
    {
        $orders = auth()->user()->orders()
            ->where('id', $id)->get();
        return view('customer.detail-orders')->with(compact('orders'));
    }

    public function update(Request $request, $id)
    {
        $customer = auth()->user()->orders()->findOrFail($id);
        // return $customer;
        $this->validate($request, [
            'confirmation' => 'mimes:jpeg,png|max:10240'
        ]);
        // $data = array();
        if ($request->hasFile('confirmation')) {
            $data['confirmation'] = $this->savePhoto($request->file('confirmation'));
            if ($customer->confirmation !== '') 
                    $this->deletePhoto($customer->confirmation);
        }

        $customer->update($data);

        \Flash::success('Berhasil di update.');
        return redirect()->route('customers.index');
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img/confirmation';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = public_path() . DIRECTORY_SEPARATOR . 'img/confirmation'
            . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }
}
