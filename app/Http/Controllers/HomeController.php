<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Order;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $c = Category::get()->count();
        // $p = Product::get()->count();
        // $o = Order::get()->count();
        // $u = User::where('role', '!=', 'admin')->count();
        return view('home');
        // , compact('c', 'p', 'o', 'u')
    }
}
