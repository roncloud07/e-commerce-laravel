<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Support\CountService;

class CountComposer {

    public function __construct(CountService $count)
    {
        $this->count = $count;
    }

    public function compose(View $view)
    {
        $view->with('count', $this->count);
    }
}
