<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fee;

class Product extends Model
{
    // protected $fillable = ['name', 'photo', 'model', 'price', 'weight'];
    protected $fillable = ['name', 'price', 'stock', 'discount', 'weight', 'photo', 'detail'];
    
    protected $appends = ['photo_path', 'price_now'];

    public static function boot()
    {
        parent::boot();

        static::deleting(function($model) {
            // remove relations to category
            $model->categories()->detach();

            // remove relation to cart
            // $model->carts()->detach();
        });
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function recommendations()
    {
        return $this->hasMany('App\Recommendation');
    }

    /**
     * Get path to product photo or give placeholder if its not set
     * @return string
     */
    public function getPhotoPathAttribute()
    {
        if ($this->photo !== '') {
            return url('/img/products/' . $this->photo);
        } else {
            return 'http://placehold.it/226x225';
        }
    }

    public function carts()
    {
        return $this->hasMany('App\Cart');
    }

    public function getCategoryListsAttribute()
    {
        if ($this->categories()->count() < 1) {
            return null;
        }
        return $this->categories->lists('id')->all();
    }

    public function getCostTo($destination_id)
    {
        return Fee::getCost(
            config('rajaongkir.origin'),
            $destination_id,
            $this->weight,
            config('rajaongkir.courier'),
            config('rajaongkir.service')
        );
    }

    public function getPriceNowAttribute()
    {
        $discount = ($this->price*$this->discount)/100;
        return $this->price - $discount;
        // buat field baru di database
    }

    public static function bestSellers()
    {
        return \DB::select(\DB::raw(
                    "SELECT products.* 
                    FROM order_details JOIN products 
                    WHERE order_details.product_id = products.id 
                    GROUP BY order_details.product_id
                    ORDER BY sum(order_details.quantity) DESC
                    LIMIT 4"
                ));
    }

}
