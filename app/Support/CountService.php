<?php

namespace App\Support;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Product;
use App\Order;
use App\User;

class CountService {

    public function cc()
    {
        return Category::get()->count();
        
    }public function pc()
    {
        return Product::get()->count();
        
    }public function oc()
    {
        return Order::get()->count();
        
    }public function uc()
    {
        return User::where('role', '!=', 'admin')->count();
        
    }
}
