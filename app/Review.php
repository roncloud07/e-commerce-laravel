<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	protected $fillable = ['rating', 'judul', 'review'];

    public function products()
    {
    	return $this->belongsTo('App\Product');
    }

    public function users()
    {
    	return $this->belongsTo('App\User');
    }
}
