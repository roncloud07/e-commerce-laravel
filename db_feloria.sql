-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2017 at 06:54 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_feloria`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `regency_id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `name`, `address`, `regency_id`, `phone`) VALUES
(1, 21, 'Ross Howell', '47980 Kulas Rue Apt. 028\nAnkundingfurt, MO 27892', '400', '901.436.1308'),
(2, 28, 'Keagan Wyman', '2023 Abshire Drive Apt. 340\nWolfftown, NE 47760', '226', '(647) 296-4261'),
(3, 24, 'Emmanuelle Durgan', '83916 Predovic Overpass\nJustinafurt, MS 74121-3904', '268', '864-974-9487'),
(4, 28, 'Prof. Myrtle Kris V', '185 Lynch Branch Apt. 549\nBettieport, FL 36480-0364', '320', '1-465-742-2148 x65412'),
(5, 17, 'Marge Hickle', '70932 Jaskolski Brook Suite 788\nThorastad, IL 66348', '350', '+1-545-787-0768'),
(6, 16, 'Koby Borer Jr.', '133 Jessyca Cliff Suite 382\nMurrayborough, CT 88654-5071', '370', '882.881.6781 x0125'),
(7, 24, 'Darius Murray', '7133 Langworth Hollow Suite 344\nNorth Izaiahville, MS 75305-5941', '317', '+1 (318) 283-5371'),
(8, 8, 'Dr. Olaf Swift Sr.', '2867 Marilou Way\nSouth Maximillianhaven, MD 69785-5417', '423', '1-619-614-2886'),
(9, 19, 'Dovie King', '5696 Max Ports\nDevanmouth, TX 28966-3372', '92', '209-615-2682 x7603'),
(10, 32, 'Kali Labadie', '7496 Vivianne Turnpike\nClemenschester, CO 43258', '276', '+1-305-931-6298'),
(11, 26, 'Rafaela Jaskolski', '5643 Hills Trace\nSchadenmouth, NC 86378', '347', '+1-591-537-3328'),
(12, 10, 'Prof. Neil Wiegand', '117 Murray Extensions Suite 743\nBrakusstad, OK 69877-7831', '475', '287.282.9973 x741'),
(13, 19, 'Ms. Desiree Rogahn III', '997 Dejah Prairie Apt. 481\nWehnertown, AL 76415-6270', '2', '1-471-880-1959 x310'),
(14, 5, 'Albina Cronin', '11171 Simeon Prairie Suite 173\nNorth Brandtstad, FL 05393', '318', '(254) 335-4454 x2127'),
(15, 21, 'Devan Fay', '626 Dylan Ports Apt. 364\nSchummland, KY 61341', '232', '1-316-552-0038 x444'),
(16, 29, 'Marquis Deckow', '889 Little Place\nGrahamhaven, AZ 00233', '352', '834.624.3262 x254'),
(17, 9, 'Prof. Anna Daniel', '10843 Russel Cliffs\nLake Kieltown, LA 69751-7465', '456', '569.716.9433 x5677'),
(18, 20, 'Salvador Will DVM', '17634 Walker Falls\nNew Daphneyland, AL 36561', '372', '970-374-9130'),
(19, 3, 'Martine Gulgowski', '5159 Stokes Overpass Suite 198\nNorth Madgetown, NM 63930', '348', '261-979-7734'),
(20, 23, 'Janick Welch DDS', '620 Hope Wall Apt. 611\nBonniemouth, MO 79241-7118', '427', '+1.494.766.8703'),
(21, 12, 'Prof. Bartholome Zieme', '65819 Lind Burg\nRosettaborough, CT 60942-4397', '328', '1-959-574-7677 x533'),
(22, 7, 'Alfreda Monahan', '95325 Alia Turnpike Suite 829\nMontanaville, NM 33419-3361', '313', '813-732-7168 x77499'),
(23, 25, 'Mr. Devon Feil MD', '89271 Emiliano Branch\nKulasburgh, ID 75771', '68', '+1-865-403-5886'),
(24, 18, 'Arielle Bergnaum', '482 Wilton Spur\nSouth Caden, SC 75238', '76', '784-332-1420'),
(25, 20, 'Lawson Collins', '40587 Reanna Estate\nSpinkachester, RI 62135', '492', '1-339-994-1990 x9200'),
(26, 22, 'Zander Bartell', '1455 Jeffery Meadows\nBatzstad, AZ 80849', '15', '1-985-974-0204'),
(27, 18, 'Justina VonRueden', '3401 Zoila Terrace Suite 412\nLednertown, DE 47298', '270', '(451) 374-0525 x2343'),
(28, 4, 'Mr. Berry Swift', '85243 Bahringer Green Apt. 244\nDuBuquemouth, OK 72667', '176', '(208) 882-4558 x792'),
(29, 27, 'Prof. Mackenzie Mohr', '6032 Lilliana Court\nNew Jazlynburgh, VT 36089-0174', '325', '219.267.6873 x40227'),
(30, 25, 'Garrett Smitham', '1763 Adeline Circle\nCronaburgh, DE 26165', '355', '1-356-258-0236 x78178'),
(31, 5, 'Mr. Myles Emard MD', '25240 Hettinger Pass\nMireyafurt, PA 08795', '327', '+1-479-662-2473'),
(32, 6, 'Dr. Emiliano Osinski Sr.', '815 Margaret Loaf\nLake Jaylonton, NH 03209', '99', '1-868-760-8936 x046'),
(33, 23, 'Sterling Terry MD', '20270 Grant Summit Apt. 579\nLake Manleytown, WI 20869-3047', '200', '(284) 203-6613 x9711'),
(34, 8, 'Theresia Spencer', '987 Waters Run Apt. 585\nJazlynberg, FL 57495-7013', '205', '(659) 672-2897 x03417'),
(35, 9, 'Mona Hammes', '42495 Zoie Via\nRutherfordshire, IL 51341', '114', '741.401.8046 x56139'),
(36, 15, 'Prof. Chandler Keeling Jr.', '358 Feest Fort Apt. 775\nRayberg, DE 09163-4556', '221', '(378) 520-5753 x295'),
(37, 2, 'Alford Hintz', '7444 Mitchell Crest\nTorpview, WV 54050', '494', '530.794.8774 x25530'),
(38, 25, 'Everette Rau', '91993 Pedro Bridge Apt. 067\nJacobsville, MO 07945-0153', '444', '+1.692.946.7765'),
(39, 23, 'Jadyn Labadie', '5737 Gutkowski Flats Apt. 280\nPort Beaulah, MO 97837', '7', '416-638-0381'),
(40, 24, 'Mrs. Madie Gutkowski', '6461 Josianne Coves\nLake Gregoriaborough, WA 73500', '418', '732.681.7884 x8225'),
(41, 17, 'Hailie McKenzie', '4463 Treutel Light\nNorth Aileen, SD 49158', '79', '896.713.4987 x034'),
(42, 8, 'Nora Raynor', '350 Edwin Track\nLake Deangelofort, VT 35838-4670', '443', '1-358-778-4907 x28500'),
(43, 7, 'Jenifer Schmidt', '496 Eladio Circles Apt. 121\nNyahborough, RI 58778-4482', '381', '916-829-2287'),
(44, 24, 'Mr. Lorenz Zulauf DVM', '351 Kassulke Drive Suite 926\nFidelborough, MT 64958-1599', '64', '+1.418.346.4549'),
(45, 30, 'Christ Franecki', '43717 Isobel Parkways\nKlingfurt, WA 84511-9185', '334', '303.827.7538 x646'),
(46, 10, 'Godfrey Dare', '5284 Bailey Light Suite 244\nGulgowskiport, MS 98157', '78', '+1-914-552-3483'),
(47, 16, 'Vladimir Davis', '8174 Romaguera Trail Suite 160\nCorinehaven, NJ 33192', '109', '283.410.5702 x70146'),
(48, 10, 'Nora Franecki', '8746 Tia Road\nViolettechester, CO 95123', '34', '+1-568-587-8903'),
(49, 3, 'Prof. Felton Mante V', '500 Curt Ports Apt. 040\nNew Clarissa, ID 47957', '131', '(473) 332-9839 x3705'),
(50, 23, 'Kamille Little V', '434 Kutch Cape Suite 834\nWest Delphine, IA 38361-6729', '227', '(995) 896-4009'),
(51, 12, 'Jessika Weimann', '37186 Runolfsdottir Points\nLizzieburgh, VT 62874-4674', '69', '823.463.8283'),
(52, 2, 'Lavada Lueilwitz', '3674 Mariana Lakes Suite 802\nSouth Faytown, AK 86419', '203', '(359) 388-7925 x7102'),
(53, 19, 'Ms. Una Tremblay IV', '20605 Ward Turnpike\nEast Roel, KY 79221', '346', '1-989-658-3369'),
(54, 9, 'Nyasia Towne', '2455 Easter Rapids\nAldaland, PA 00731-0098', '261', '(215) 303-2009 x8752'),
(55, 17, 'Curtis Gaylord', '11864 Lizeth Prairie\nEast Vidalshire, ND 87579', '134', '(248) 913-7160 x0001'),
(56, 26, 'Mr. Devin Ortiz', '532 Hane Bridge Apt. 220\nPort Caitlynfort, NY 30041', '447', '1-426-532-3228 x3821'),
(57, 10, 'Mike Predovic', '7155 Lindsay Grove Apt. 450\nGrantburgh, NY 71423-7343', '354', '343-202-5128 x87807'),
(58, 21, 'Jazlyn Raynor DDS', '719 Mills Valley Apt. 658\nLake Lowell, FL 39644-1777', '57', '1-607-906-2368'),
(59, 23, 'Prof. Kristoffer Hand Jr.', '40456 Coralie Forks Suite 541\nSchillerview, ND 25726', '179', '581.401.3690'),
(60, 10, 'Kylie Jenkins', '75527 Caden Lodge\nLake Graysonport, KS 39435-6863', '139', '1-783-819-1004 x51952'),
(61, 30, 'Lexi Haag', '21374 George Shores\nHaleystad, TX 95825-4900', '30', '1-229-558-7153'),
(62, 22, 'Skyla Dooley', '848 Spencer Estates\nNorth Noelia, NE 21826', '491', '635.400.6317 x044'),
(63, 24, 'Fletcher Krajcik II', '71900 Dach Mills Apt. 936\nSouth Stuart, CT 64260', '80', '301.371.9658 x517'),
(64, 10, 'Myrna Greenholt IV', '7020 Wehner Harbor\nSouth Joanberg, MN 61808', '156', '1-862-639-7478 x48358'),
(65, 9, 'Mr. Hyman McKenzie', '1480 Laron Parkways Suite 672\nEast Carterberg, MO 55481', '250', '834-735-3710'),
(66, 3, 'Prof. Mazie Cartwright', '238 Breitenberg Skyway\nVicentaborough, UT 42274', '435', '424-446-6217'),
(67, 10, 'Jessika Krajcik', '64657 Dion Mount Apt. 711\nLake Anabelle, MD 41545', '211', '+1-239-946-0589'),
(68, 17, 'Nia Rath III', '71907 Dana Pike\nRivertown, AR 41696', '373', '1-889-883-1383'),
(69, 2, 'Celestine Goyette', '85338 Marks Mount Suite 894\nHirtheside, GA 60180', '387', '394.977.6629'),
(70, 19, 'Ms. Euna Williamson', '9917 Balistreri Manors Apt. 909\nMarvinport, NH 47874', '267', '+1-575-761-7916'),
(71, 6, 'Orrin Kuhn', '56260 Torrance Track Suite 313\nBarneyburgh, WA 35323-2111', '448', '965.539.5859'),
(72, 24, 'Prof. Christ Bernhard', '55788 Jasmin Glens\nSouth Madonnaport, MT 82482-5825', '163', '(276) 254-1242 x11234'),
(73, 31, 'Bianka Pfannerstill', '9319 Fritsch Neck Apt. 784\nNorth Janae, OK 21417', '317', '1-454-516-5761 x1196'),
(74, 31, 'Quinton Mueller', '3047 Gottlieb Crest\nCassinburgh, NM 99435-9230', '360', '832-919-2400 x49285'),
(75, 4, 'Mrs. Dorothea Douglas', '99743 Audreanne Street\nO''Konhaven, NE 84807', '165', '1-684-226-8195 x0369'),
(76, 23, 'Margarette Bashirian', '948 Cronin Drives\nKreigerfurt, LA 26752', '403', '980.466.5552 x2408'),
(77, 26, 'Dr. Roman Jerde', '92900 Frami Views\nEast Estebantown, NC 00825-6820', '415', '271-485-1696'),
(78, 31, 'Green Bernhard', '993 Dickinson Knolls\nKiehnville, WI 83835', '89', '325.639.4007 x6051'),
(79, 27, 'Kasandra Braun', '47231 Veum Lights Suite 788\nEast Reggie, NJ 40635', '238', '486.677.6412'),
(80, 6, 'Montana Pacocha', '500 Pacocha Loop Suite 579\nWest Dashawn, NY 39076-4872', '467', '817.395.1190'),
(81, 12, 'Malachi Kertzmann V', '973 Greg Roads\nRogahnton, NV 64458-1974', '55', '1-706-788-8699 x2607'),
(82, 11, 'Kamron Grimes', '219 Bernier Street\nNorth Guiseppe, IL 54843', '331', '746-321-1073 x0949'),
(83, 5, 'Mrs. Keely Olson', '2336 Blaze Circles Suite 832\nNorth Marina, VA 55308', '40', '(275) 237-7909 x706'),
(84, 28, 'Dr. Marc Kilback III', '643 Padberg Forks\nPort Alvisville, WI 97018', '230', '502-397-0275 x5471'),
(85, 11, 'Kamron Waelchi PhD', '1590 Stiedemann Lodge\nAlexzanderbury, ID 77244-5471', '114', '+1-412-541-9888'),
(86, 26, 'Tessie Homenick', '51776 Ebony Ford Suite 487\nLake Masontown, IL 87830', '460', '1-404-800-9764 x532'),
(87, 23, 'Filiberto Doyle', '1655 Emery Knolls\nLake Thaddeus, MO 68688-5684', '450', '957-368-9280 x8951'),
(88, 18, 'Mr. Vinnie Lehner', '310 Nona Walk Suite 715\nWest Khalil, NV 04243', '67', '(632) 629-8404 x333'),
(89, 16, 'Christiana Ferry Jr.', '13873 Deckow Mountain\nDanielberg, MD 38530-5450', '467', '+14948600120'),
(90, 5, 'Vita Becker', '3919 Padberg Wells Apt. 871\nChristiansenburgh, TX 11031-9457', '350', '345-339-9830 x532'),
(91, 12, 'Leone Rohan', '23146 Bryce Point\nNew Bryanaport, TX 35992-4807', '339', '+12802903741'),
(92, 32, 'Lexie McCullough', '562 Pouros Causeway Suite 831\nMoenport, CT 37677-5350', '354', '812.234.0562 x9849'),
(93, 19, 'Gideon D''Amore', '9828 Stark Stravenue\nModestoburgh, NE 24633-4198', '487', '932-640-6910 x0434'),
(94, 4, 'Prof. Trycia O''Kon I', '73779 Eldridge Park Apt. 376\nSouth Reginaldport, IA 74782', '376', '340.604.2114'),
(95, 26, 'Eleanora Macejkovic', '5378 Sonya Mountains Suite 263\nDestinchester, NJ 13771', '83', '+1-928-307-9301'),
(96, 30, 'Denis Robel', '844 Lenora Falls\nLake Mandy, CT 46311', '465', '245.906.7694 x60665'),
(97, 11, 'Yadira Hessel I', '847 Hank Mall\nNorth Kaycee, AK 92979-6298', '49', '1-240-727-4353 x862'),
(98, 14, 'Vita Tromp', '59555 Duncan Shoals Apt. 571\nLeschmouth, RI 16775', '415', '+1-934-798-4534'),
(99, 27, 'Dr. Moriah Gleason', '863 Funk Tunnel Suite 546\nKamillestad, NV 09786', '57', '676-727-0976'),
(100, 4, 'Giovanna Lindgren', '15120 Hilll Port Suite 760\nSipesshire, IL 74423', '274', '(392) 935-6091'),
(101, 17, 'Prof. Nicklaus Jones MD', '57320 Aufderhar Village\nGregoriahaven, AK 50905-9367', '30', '+1-772-897-4425'),
(102, 28, 'Antonia Cartwright PhD', '547 Paucek Inlet Apt. 204\nNew Albertaside, WI 94419-4868', '340', '393.834.3568'),
(103, 5, 'Sigrid Wehner', '77843 Ollie Wells Suite 249\nReichelport, FL 58817-5522', '138', '+1 (790) 832-9739'),
(104, 9, 'Vivianne Brekke', '9273 Micah Road\nBrooksborough, VT 18086-3220', '158', '(614) 833-5194 x33280'),
(105, 29, 'Bernard Frami', '2225 Wunsch Stravenue Apt. 678\nEast Elwyn, NH 03201', '304', '+1-235-465-9446'),
(106, 7, 'Cordia Schimmel IV', '77519 Heidenreich Walk\nAnnaliseton, CT 84684', '26', '+18248323147'),
(107, 17, 'Mrs. Hailie Frami DDS', '28851 Prohaska Stream\nEast Adella, NY 30096', '199', '(267) 371-4677 x6939'),
(108, 6, 'Lew Bednar', '630 Stiedemann Mill\nWest Martin, UT 30391-9817', '492', '+15055261705'),
(109, 7, 'Barry Ratke', '9131 Hayes Road Apt. 364\nLake Yazmin, AR 28671-6151', '246', '371.233.3910 x9685'),
(110, 5, 'Eulalia Heller Sr.', '922 Lafayette Lake\nNorth Jonathonbury, NJ 89088-4373', '350', '919-755-8199 x5444'),
(111, 22, 'Lane Macejkovic', '8169 Christiansen Plaza Apt. 451\nMarianoville, NV 38276', '115', '546-653-9837'),
(112, 18, 'Jamel Runolfsson', '64328 Alva Terrace\nLake Urbanstad, OK 63921-4801', '149', '1-732-806-8491'),
(113, 6, 'Mr. Ansley Cormier', '4460 Kub Cliff\nWainofurt, RI 20027-7415', '230', '(342) 388-0111'),
(114, 14, 'Lauriane Kutch', '1581 Cummerata Highway Suite 316\nLinniechester, GA 66186-2337', '185', '1-503-950-3482 x116'),
(115, 14, 'Sebastian Kertzmann', '80429 Marty Harbor Apt. 949\nLake Carolanne, ND 46311-1903', '262', '758-249-7883 x5029'),
(116, 7, 'Adan Swift', '39586 Willms Streets\nDaynashire, MD 04766-9271', '6', '574.982.9980 x279'),
(117, 19, 'Clay Collier', '8284 Ocie Squares Suite 823\nEast Granvilleborough, OR 38700-3927', '133', '864-314-1737'),
(118, 27, 'Prof. Kacie Halvorson', '9869 Noemie Mountains\nLake Evelyn, VT 47376-1548', '408', '579.266.6251'),
(119, 14, 'Prof. Angelina Ankunding', '944 London Corner\nWest Aidashire, GA 25785', '14', '696-913-0849'),
(120, 27, 'Mrs. Velda Gislason', '3140 Harry Centers\nWest John, VA 82564', '437', '(752) 379-0116 x588'),
(121, 6, 'Dr. Edgar Crooks', '276 Moore Track Apt. 337\nNew Kory, DE 71708-9285', '329', '(647) 855-4110 x789'),
(122, 5, 'Eliane Beahan', '9119 Kennedi Alley Suite 637\nJettiestad, DC 77060-2998', '392', '(869) 354-8054'),
(123, 17, 'Nova Koss', '8310 Schinner Pines\nLolitaview, KS 97411', '248', '393.298.6211 x6871'),
(124, 7, 'Electa Nicolas', '3202 Crona Plaza\nJohathanview, MI 75845-9039', '317', '(397) 621-9695'),
(125, 22, 'Marcel Kertzmann IV', '9696 Beier Port\nWindlerborough, VT 36214', '192', '371-202-0122 x3466'),
(126, 2, 'Lukas Lindgren', '4479 Rohan Manor Suite 938\nSouth Louisaport, AL 03140-8951', '11', '547-417-2947'),
(127, 14, 'Terence Predovic', '52871 Jenkins Ridges\nNew Jamalshire, MS 52580', '336', '1-475-997-1548'),
(128, 3, 'Kaya Hilll', '376 Willms Spur\nHermistonhaven, ND 06069-2190', '21', '406-433-9549'),
(129, 29, 'Elisabeth Cummerata', '171 Klein Island\nNorth Ignacioton, FL 15675-5201', '99', '1-859-312-3883 x5391'),
(130, 10, 'Obie Kuhic', '356 Adeline Common\nPort Dessie, VA 38451-1278', '144', '1-316-496-5014 x25588'),
(131, 28, 'Angelina Von', '2981 Marcel Greens Suite 235\nNew Isai, ND 74404', '154', '(203) 586-6450 x190'),
(132, 6, 'Prof. Edd Jerde II', '602 Alvena Land\nWest Madisyn, MO 22601-7278', '426', '(579) 341-9244 x94886'),
(133, 17, 'Mr. Vicente Lynch', '978 Sawayn Views\nKattiefurt, VA 44803-9354', '424', '875-739-1885'),
(134, 22, 'Dr. Alisa Lind III', '8668 Douglas Avenue\nDianabury, WV 83452', '163', '+1 (660) 243-0274'),
(135, 20, 'Cheyenne Marquardt', '886 Hermann Trail\nNelsonton, MO 68435', '78', '817.391.6022 x090'),
(136, 9, 'Marta Rau', '46926 Kub Common Apt. 553\nLednershire, FL 52496-6515', '404', '1-356-796-7297 x89397'),
(137, 11, 'Glenda Rogahn', '6468 Erdman Place Suite 513\nNataliemouth, OH 27787-3981', '41', '(247) 204-9180 x820'),
(138, 30, 'Katarina Dicki III', '98640 Wilmer Club Apt. 334\nEast Margarette, NE 21951', '444', '764-807-0084'),
(139, 14, 'Demond Gulgowski', '888 Emory Knoll Apt. 717\nGreenfelderborough, MA 53880-6764', '484', '859.742.3725 x979'),
(140, 4, 'Allen Lueilwitz', '166 Gracie Alley Suite 306\nLabadieshire, DE 32734-2186', '331', '+1-495-545-9458'),
(141, 3, 'Marjolaine Schulist', '8200 Hailie Well\nEast Waylonport, ID 44812', '327', '970-381-0948 x2240'),
(142, 26, 'Zachary Ruecker I', '4540 Lehner Manors\nLeannonmouth, GA 89978-6183', '361', '+1 (637) 708-9662'),
(143, 2, 'Prof. Abbigail Sauer', '232 Kohler Creek Apt. 211\nIsmaelfort, NY 19816', '243', '+16266344773'),
(144, 15, 'Mrs. Jane Sawayn Jr.', '60408 Johnpaul Parks\nNorth Erikafurt, MD 59765-1286', '82', '+1 (947) 527-8781'),
(145, 16, 'Davonte Heathcote', '48710 Wisozk Club\nWest Brendanstad, NE 17609', '155', '295-926-8221'),
(146, 2, 'Morgan Wolff', '666 Marlon Alley Apt. 878\nOthochester, WV 44701', '290', '258.376.1930'),
(147, 7, 'Nova Langworth DVM', '122 Wiegand Square\nConorshire, DE 76679-3020', '234', '(682) 899-3735'),
(148, 2, 'Holden Hand', '499 Cartwright Forks Suite 975\nEast Vincentborough, DE 51864-3316', '32', '406-484-0178 x60204'),
(149, 11, 'Prof. Monique Grant III', '1252 Xander Motorway Apt. 065\nPort Dessie, AZ 93060', '402', '+1.998.856.7400'),
(150, 13, 'Nichole Jenkins', '26701 Brandon Street\nHowechester, NE 35322-5565', '106', '831-774-1037 x10976');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, '2016-12-27 14:42:59', '2016-12-27 14:42:59'),
(12, 33, 10, 8, '2017-01-18 16:05:28', '2017-01-18 16:05:28'),
(13, 33, 25, 3, '2017-01-18 16:05:28', '2017-01-18 16:05:28'),
(14, 33, 69, 1, '2017-01-18 16:05:28', '2017-01-18 16:05:28'),
(15, 16, 30, 1, '2017-04-03 08:27:06', '2017-04-03 08:27:06'),
(16, 16, 68, 1, '2017-04-03 10:34:55', '2017-04-03 10:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `parent_id`) VALUES
(1, 'Furniture', 0),
(2, 'Office Supplies', 0),
(3, 'Technology', 0),
(4, 'Appliances', 2),
(5, 'Binders and Binder Accessories', 2),
(6, 'Bookcases', 1),
(7, 'Chairs & Chairmats', 1),
(8, 'Computer Peripherals', 3),
(9, 'Labels', 2),
(10, 'Office Furnishings', 1),
(11, 'Office Machines', 3),
(12, 'Paper', 2),
(13, 'Pens & Art Supplies', 2),
(14, 'Rubber Bands', 2),
(15, 'Scissors, Rulers and Trimmers', 2),
(16, 'Storage & Organization', 2),
(17, 'Tables', 1),
(18, 'Telephones and Communication', 3);

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

CREATE TABLE `category_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `product_id`, `category_id`) VALUES
(1, 1, 18),
(2, 2, 5),
(3, 3, 12),
(4, 4, 9),
(5, 5, 5),
(6, 6, 5),
(7, 7, 13),
(8, 8, 8),
(9, 9, 17),
(10, 10, 7),
(11, 11, 13),
(12, 12, 12),
(13, 13, 17),
(14, 14, 5),
(15, 15, 5),
(16, 16, 17),
(17, 17, 16),
(18, 18, 18),
(19, 19, 10),
(20, 20, 10),
(21, 21, 8),
(22, 22, 5),
(23, 23, 5),
(24, 24, 15),
(25, 25, 15),
(26, 26, 5),
(27, 27, 5),
(28, 28, 10),
(29, 29, 17),
(30, 30, 7),
(31, 31, 7),
(32, 32, 7),
(33, 33, 7),
(34, 34, 11),
(35, 35, 11),
(36, 36, 4),
(37, 37, 17),
(38, 38, 7),
(39, 39, 5),
(40, 40, 8),
(41, 41, 8),
(42, 42, 10),
(43, 43, 8),
(44, 44, 13),
(45, 45, 13),
(46, 46, 7),
(47, 47, 5),
(48, 48, 5),
(49, 49, 16),
(50, 50, 12),
(51, 51, 17),
(52, 52, 6),
(53, 53, 5),
(54, 54, 14),
(55, 55, 14),
(56, 56, 18),
(57, 57, 10),
(58, 58, 10),
(59, 59, 16),
(60, 60, 16),
(61, 61, 12),
(62, 62, 5),
(63, 63, 5),
(64, 64, 12),
(65, 65, 12),
(66, 66, 12),
(67, 67, 12),
(68, 68, 12),
(69, 69, 12),
(70, 70, 12),
(71, 71, 12);

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `origin` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  `courier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`id`, `origin`, `destination`, `courier`, `service`, `weight`, `cost`, `created_at`, `updated_at`) VALUES
(3, 23, 386, 'jne', 'REG', 21000, 399000, '2017-01-16 19:41:57', '2017-01-16 00:00:00'),
(4, 23, 386, 'jne', 'REG', 1000, 22000, '2017-01-16 19:41:58', '2017-01-16 00:00:00'),
(5, 23, 370, 'jne', 'REG', 3000, 66000, '2017-01-19 00:20:30', '2017-01-19 00:00:00'),
(6, 23, 370, 'jne', 'REG', 4000, 88000, '2017-01-19 00:20:31', '2017-01-19 00:00:00'),
(7, 23, 155, 'jne', 'REG', 3000, 30000, '2017-01-19 00:27:06', '2017-01-19 00:00:00'),
(8, 23, 155, 'jne', 'REG', 4000, 40000, '2017-01-19 00:27:07', '2017-01-19 00:00:00'),
(9, 23, 109, 'jne', 'REG', 5000, 55000, '2017-01-19 00:34:20', '2017-01-19 00:00:00'),
(10, 23, 155, 'jne', 'REG', 5000, 50000, '2017-01-19 00:39:04', '2017-01-19 00:00:00'),
(11, 23, 467, 'jne', 'REG', 5000, 195000, '2017-01-19 00:40:35', '2017-01-19 00:00:00'),
(12, 23, 370, 'jne', 'REG', 5000, 110000, '2017-01-19 01:06:47', '2017-01-19 00:00:00'),
(13, 23, 325, 'jne', 'REG', 8000, 40000, '2017-01-22 23:42:47', '2017-01-22 00:00:00'),
(14, 23, 325, 'jne', 'REG', 27000, 40000, '2017-01-22 23:42:48', '2017-01-22 00:00:00'),
(15, 23, 109, 'jne', 'REG', 1000, 11000, '2017-01-23 09:04:23', '2017-01-23 00:00:00'),
(16, 23, 370, 'jne', 'REG', 26000, 572000, '2017-01-23 10:13:29', '2017-01-23 00:00:00'),
(17, 23, 325, 'jne', 'REG', 21000, 40000, '2017-01-23 10:15:27', '2017-01-23 00:00:00'),
(18, 23, 467, 'jne', 'REG', 27000, 1053000, '2017-01-23 19:02:00', '2017-01-23 00:00:00'),
(19, 23, 370, 'jne', 'REG', 16000, 352000, '2017-01-23 19:02:23', '2017-01-23 00:00:00'),
(20, 23, 435, 'jne', 'REG', 14000, 40000, '2017-01-23 19:07:19', '2017-01-23 00:00:00'),
(21, 23, 348, 'jne', 'REG', 21000, 399000, '2017-01-23 19:07:41', '2017-01-23 00:00:00'),
(22, 23, 348, 'jne', 'REG', 12000, 228000, '2017-01-23 19:08:09', '2017-01-23 00:00:00'),
(23, 23, 348, 'jne', 'REG', 19000, 361000, '2017-01-23 19:08:43', '2017-01-23 00:00:00'),
(24, 23, 348, 'jne', 'REG', 24000, 456000, '2017-01-23 19:08:45', '2017-01-23 00:00:00'),
(25, 23, 325, 'jne', 'REG', 3000, 40000, '2017-01-23 19:14:29', '2017-01-23 00:00:00'),
(26, 23, 325, 'jne', 'REG', 4000, 40000, '2017-01-23 19:14:29', '2017-01-23 00:00:00'),
(27, 23, 325, 'jne', 'REG', 17000, 40000, '2017-01-23 19:14:29', '2017-01-23 00:00:00'),
(28, 23, 238, 'jne', 'REG', 26000, 40000, '2017-01-23 19:18:30', '2017-01-23 00:00:00'),
(29, 23, 238, 'jne', 'REG', 20000, 40000, '2017-01-23 19:18:30', '2017-01-23 00:00:00'),
(30, 23, 131, 'jne', 'REG', 3000, 144000, '2017-01-23 19:52:41', '2017-01-23 00:00:00'),
(31, 23, 57, 'jne', 'REG', 26000, 40000, '2017-01-24 13:25:21', '2017-01-24 00:00:00'),
(32, 23, 325, 'jne', 'REG', 14000, 40000, '2017-01-24 13:34:31', '2017-01-24 00:00:00'),
(33, 23, 325, 'jne', 'REG', 12000, 40000, '2017-01-24 13:34:32', '2017-01-24 00:00:00'),
(34, 23, 467, 'jne', 'REG', 26000, 1014000, '2017-01-24 13:39:52', '2017-01-24 00:00:00'),
(35, 23, 325, 'jne', 'REG', 26000, 40000, '2017-01-24 13:42:04', '2017-01-24 00:00:00'),
(36, 23, 325, 'jne', 'REG', 5000, 40000, '2017-01-24 13:42:04', '2017-01-24 00:00:00'),
(37, 23, 109, 'jne', 'REG', 26000, 286000, '2017-01-24 13:43:03', '2017-01-24 00:00:00'),
(38, 23, 325, 'jne', 'REG', 25000, 40000, '2017-01-24 14:33:42', '2017-01-24 00:00:00'),
(39, 23, 370, 'jne', 'REG', 25000, 550000, '2017-01-24 14:37:54', '2017-01-24 00:00:00'),
(40, 23, 408, 'jne', 'REG', 3000, 141000, '2017-01-24 14:39:10', '2017-01-24 00:00:00'),
(41, 23, 408, 'jne', 'REG', 14000, 658000, '2017-01-24 14:39:11', '2017-01-24 00:00:00'),
(42, 23, 370, 'jne', 'REG', 14000, 308000, '2017-01-25 10:05:32', '2017-01-25 00:00:00'),
(43, 23, 57, 'jne', 'REG', 14000, 40000, '2017-01-25 10:06:22', '2017-01-25 00:00:00'),
(44, 23, 57, 'jne', 'REG', 7000, 40000, '2017-01-25 10:06:22', '2017-01-25 00:00:00'),
(45, 23, 238, 'jne', 'REG', 3000, 40000, '2017-01-25 10:09:59', '2017-01-25 00:00:00'),
(46, 23, 370, 'jne', 'REG', 21000, 462000, '2017-01-25 10:50:03', '2017-01-25 00:00:00'),
(47, 23, 325, 'jne', 'REG', 6000, 40000, '2017-01-25 10:51:11', '2017-01-25 00:00:00'),
(48, 23, 348, 'jne', 'REG', 17000, 323000, '2017-01-25 10:52:18', '2017-01-25 00:00:00'),
(49, 23, 131, 'jne', 'REG', 26000, 1248000, '2017-01-25 10:52:58', '2017-01-25 00:00:00'),
(50, 23, 131, 'jne', 'REG', 12000, 576000, '2017-01-25 10:52:59', '2017-01-25 00:00:00'),
(51, 23, 370, 'jne', 'REG', 6000, 132000, '2017-01-25 10:54:35', '2017-01-25 00:00:00'),
(52, 23, 370, 'jne', 'REG', 12000, 264000, '2017-01-25 10:55:56', '2017-01-25 00:00:00'),
(53, 23, 370, 'jne', 'REG', 19000, 418000, '2017-01-25 11:02:37', '2017-01-25 00:00:00'),
(54, 23, 109, 'jne', 'REG', 19000, 209000, '2017-01-25 11:03:47', '2017-01-25 00:00:00'),
(55, 23, 354, 'jne', 'REG', 19000, 1007000, '2017-01-25 11:15:04', '2017-01-25 00:00:00'),
(56, 23, 354, 'jne', 'REG', 14000, 742000, '2017-01-25 11:15:08', '2017-01-25 00:00:00'),
(57, 23, 354, 'jne', 'REG', 5000, 265000, '2017-01-25 11:15:11', '2017-01-25 00:00:00'),
(58, 23, 347, 'jne', 'REG', 3000, 40000, '2017-01-25 11:16:31', '2017-01-25 00:00:00'),
(59, 23, 347, 'jne', 'REG', 25000, 40000, '2017-01-25 11:16:32', '2017-01-25 00:00:00'),
(60, 23, 92, 'jne', 'REG', 13000, 247000, '2017-01-25 11:17:27', '2017-01-25 00:00:00'),
(61, 23, 92, 'jne', 'REG', 20000, 380000, '2017-01-25 11:17:31', '2017-01-25 00:00:00'),
(62, 23, 92, 'jne', 'REG', 12000, 228000, '2017-01-25 11:17:32', '2017-01-25 00:00:00'),
(63, 23, 185, 'jne', 'REG', 3000, 40000, '2017-01-25 11:18:20', '2017-01-25 00:00:00'),
(64, 23, 185, 'jne', 'REG', 24000, 40000, '2017-01-25 11:18:21', '2017-01-25 00:00:00'),
(65, 23, 69, 'jne', 'REG', 21000, 777000, '2017-01-25 11:19:18', '2017-01-25 00:00:00'),
(66, 23, 69, 'jne', 'REG', 5000, 185000, '2017-01-25 11:19:18', '2017-01-25 00:00:00'),
(67, 23, 34, 'jne', 'REG', 21000, 357000, '2017-01-25 11:20:15', '2017-01-25 00:00:00'),
(68, 23, 34, 'jne', 'REG', 3000, 51000, '2017-01-25 11:20:16', '2017-01-25 00:00:00'),
(69, 23, 423, 'jne', 'REG', 19000, 893000, '2017-01-25 11:21:11', '2017-01-25 00:00:00'),
(70, 23, 423, 'jne', 'REG', 1000, 47000, '2017-01-25 11:21:12', '2017-01-25 00:00:00'),
(71, 23, 423, 'jne', 'REG', 26000, 1222000, '2017-01-25 11:21:14', '2017-01-25 00:00:00'),
(72, 23, 456, 'jne', 'REG', 21000, 231000, '2017-01-25 11:22:16', '2017-01-25 00:00:00'),
(73, 23, 456, 'jne', 'REG', 4000, 44000, '2017-01-25 11:22:17', '2017-01-25 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `address_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'waiting-payment',
  `bank` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sender` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `total_payment` decimal(18,2) NOT NULL,
  `confirmation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `address_id`, `status`, `bank`, `sender`, `total_payment`, `confirmation`, `created_at`, `updated_at`) VALUES
(5346, 17, 1, 'waiting-payment', 'mandiri', 'Miller', '81133000.00', '', '2015-12-31 18:30:01', '2015-12-31 18:30:02'),
(6272, 25, 2, 'waiting-payment', 'bni', 'Ferguson', '247141000.00', '', '2016-01-01 18:30:02', '2016-01-01 18:30:03'),
(7078, 26, 3, 'waiting-payment', 'mandiri', 'Block', '144986000.00', '', '2016-01-02 18:30:03', '2016-01-02 18:30:04'),
(8388, 23, 4, 'waiting-payment', 'mandiri', 'Toch', '141441000.00', '', '2016-01-03 18:30:04', '2016-01-03 18:30:05'),
(9249, 2, 5, 'waiting-payment', 'bca', 'Ballentine', '189352000.00', '', '2016-01-04 18:30:05', '2016-01-04 18:30:06'),
(10144, 29, 6, 'waiting-payment', 'bca', 'Sunley', '521842000.00', '', '2016-01-05 18:30:06', '2016-01-05 18:30:07'),
(13345, 5, 7, 'waiting-payment', 'mandiri', 'Donatelli', '10286000.00', '', '2016-01-06 18:30:07', '2016-01-06 18:30:08'),
(14855, 15, 8, 'waiting-payment', 'bca', 'Lee', '111406000.00', '', '2016-01-07 18:30:08', '2016-01-07 18:30:09'),
(15109, 18, 9, 'waiting-payment', 'mandiri', 'Bertelson', '27623000.00', '', '2016-01-08 18:30:09', '2016-01-08 18:30:10'),
(23911, 8, 10, 'waiting-payment', 'bni', 'Conant', '169846000.00', '', '2016-01-09 18:30:10', '2016-01-09 18:30:11'),
(24132, 16, 11, 'waiting-payment', 'bni', 'Knight', '23156000.00', '', '2016-01-10 18:30:11', '2016-01-10 18:30:12'),
(24672, 30, 12, 'waiting-payment', 'atm-bersama', 'Van', '189980000.00', '', '2016-01-11 18:30:12', '2016-01-11 18:30:13'),
(29187, 27, 13, 'waiting-payment', 'bni', 'Wilson', '147687000.00', '', '2016-01-12 18:30:13', '2016-01-12 18:30:14'),
(29383, 13, 14, 'waiting-payment', 'bni', 'O''Carroll', '41149000.00', '', '2016-01-13 18:30:14', '2016-01-13 18:30:15'),
(30341, 19, 15, 'waiting-payment', 'bni', 'Abelman', '234534000.00', '', '2016-01-14 18:30:15', '2016-01-14 18:30:16'),
(31938, 28, 16, 'waiting-payment', 'bca', 'Shariari', '49884000.00', '', '2016-01-15 18:30:16', '2016-01-15 18:30:17'),
(35046, 7, 17, 'waiting-payment', 'bni', 'Sayre', '127090000.00', '', '2016-01-16 18:30:17', '2016-01-16 18:30:18'),
(39876, 14, 18, 'waiting-payment', 'atm-bersama', 'Stevenson', '429732000.00', '', '2016-01-17 18:30:18', '2016-01-17 18:30:19'),
(40132, 12, 19, 'waiting-payment', 'bca', 'Glantz', '205348000.00', '', '2016-01-18 18:30:19', '2016-01-18 18:30:20'),
(43399, 6, 20, 'waiting-payment', 'bca', 'MacIntyre', '16112000.00', '', '2016-01-19 18:30:20', '2016-01-19 18:30:21'),
(44231, 3, 21, 'waiting-payment', 'bni', 'Avila', '73420000.00', '', '2016-01-20 18:30:21', '2016-01-20 18:30:22'),
(46147, 22, 22, 'waiting-payment', 'bni', 'Crebagga', '153012000.00', '', '2016-01-21 18:30:22', '2016-01-21 18:30:23'),
(46597, 20, 23, 'waiting-payment', 'bni', 'Chen', '34093000.00', '', '2016-01-22 18:30:23', '2016-01-22 18:30:24'),
(47877, 31, 24, 'waiting-payment', 'mandiri', 'Willingham', '199322000.00', '', '2016-01-23 18:30:24', '2016-01-23 18:30:25'),
(48167, 10, 25, 'waiting-payment', 'bni', 'Kaydos', '565408000.00', '', '2016-01-24 18:30:25', '2016-01-24 18:30:26'),
(49927, 24, 26, 'waiting-payment', 'bca', 'Norris', '5263000.00', '', '2016-01-25 18:30:26', '2016-01-25 18:30:27'),
(50854, 9, 27, 'waiting-payment', 'atm-bersama', 'Lawera', '13185000.00', '', '2016-01-26 18:30:27', '2016-01-26 18:30:28'),
(52867, 32, 30, 'waiting-payment', 'mandiri', 'O''Briant', '67030000.00', '', '2016-01-30 18:30:31', '2016-01-30 18:30:32'),
(52999, 11, 28, 'waiting-payment', 'bca', 'Hildebrand', '8079000.00', '', '2016-01-27 18:30:28', '2016-01-27 18:30:29'),
(55554, 4, 29, 'waiting-payment', 'bni', 'Tate', '24269000.00', '', '2016-01-28 18:30:29', '2016-01-28 18:30:30'),
(56224, 21, 30, 'waiting-payment', 'bni', 'Arnett', '114602000.00', '', '2016-01-29 18:30:30', '2016-01-29 18:30:31');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(10) UNSIGNED DEFAULT NULL,
  `price` decimal(11,2) UNSIGNED DEFAULT NULL,
  `fee` decimal(11,2) UNSIGNED DEFAULT NULL,
  `total_price` decimal(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `quantity`, `price`, `fee`, `total_price`) VALUES
(1, 5346, 19, 34, '45526000.00', '40000.00', '45566000.00'),
(2, 5346, 13, 45, '1215000.00', '40000.00', '1255000.00'),
(3, 5346, 52, 17, '34272000.00', '40000.00', '34312000.00'),
(4, 6272, 28, 33, '9801000.00', '40000.00', '9841000.00'),
(5, 6272, 9, 48, '214368000.00', '40000.00', '214408000.00'),
(6, 6272, 1, 8, '8552000.00', '40000.00', '8592000.00'),
(7, 6272, 57, 31, '14260000.00', '40000.00', '14300000.00'),
(8, 7078, 36, 9, '14490000.00', '40000.00', '14530000.00'),
(9, 7078, 15, 11, '891000.00', '40000.00', '931000.00'),
(10, 7078, 32, 16, '120832000.00', '40000.00', '120872000.00'),
(11, 7078, 28, 29, '8613000.00', '40000.00', '8653000.00'),
(12, 8388, 42, 39, '4719000.00', '40000.00', '4759000.00'),
(13, 8388, 22, 37, '133200000.00', '40000.00', '133240000.00'),
(14, 8388, 3, 42, '3402000.00', '40000.00', '3442000.00'),
(15, 9249, 7, 46, '6210000.00', '40000.00', '6250000.00'),
(16, 9249, 17, 42, '26712000.00', '40000.00', '26752000.00'),
(17, 9249, 9, 35, '156310000.00', '40000.00', '156350000.00'),
(18, 10144, 38, 16, '120400000.00', '40000.00', '120440000.00'),
(19, 10144, 46, 1, '5346000.00', '40000.00', '5386000.00'),
(20, 10144, 29, 24, '395976000.00', '40000.00', '396016000.00'),
(21, 13345, 41, 24, '7776000.00', '40000.00', '7816000.00'),
(22, 13345, 25, 45, '2430000.00', '40000.00', '2470000.00'),
(23, 14855, 6, 44, '1760000.00', '40000.00', '1800000.00'),
(24, 14855, 20, 48, '24672000.00', '40000.00', '24712000.00'),
(25, 14855, 9, 19, '84854000.00', '40000.00', '84894000.00'),
(26, 15109, 21, 11, '4763000.00', '40000.00', '4803000.00'),
(27, 15109, 68, 45, '13365000.00', '40000.00', '13405000.00'),
(28, 15109, 11, 33, '3993000.00', '40000.00', '4033000.00'),
(29, 15109, 25, 23, '1242000.00', '40000.00', '1282000.00'),
(30, 15109, 49, 10, '4060000.00', '40000.00', '4100000.00'),
(31, 23911, 32, 21, '158592000.00', '40000.00', '158632000.00'),
(32, 23911, 43, 19, '8740000.00', '40000.00', '8780000.00'),
(33, 23911, 65, 3, '2394000.00', '40000.00', '2434000.00'),
(34, 24132, 62, 4, '972000.00', '40000.00', '1012000.00'),
(35, 24132, 63, 46, '6210000.00', '40000.00', '6250000.00'),
(36, 24132, 21, 22, '9526000.00', '40000.00', '9566000.00'),
(37, 24132, 39, 31, '4588000.00', '40000.00', '4628000.00'),
(38, 24132, 53, 12, '648000.00', '40000.00', '688000.00'),
(39, 24132, 59, 3, '972000.00', '40000.00', '1012000.00'),
(40, 24672, 42, 5, '605000.00', '40000.00', '645000.00'),
(41, 24672, 64, 37, '5476000.00', '40000.00', '5516000.00'),
(42, 24672, 60, 37, '183779000.00', '40000.00', '183819000.00'),
(43, 29187, 27, 5, '1755000.00', '40000.00', '1795000.00'),
(44, 29187, 47, 8, '1184000.00', '40000.00', '1224000.00'),
(45, 29187, 34, 35, '141610000.00', '40000.00', '141650000.00'),
(46, 29187, 69, 10, '670000.00', '40000.00', '710000.00'),
(47, 29187, 25, 42, '2268000.00', '40000.00', '2308000.00'),
(48, 29383, 40, 20, '1080000.00', '40000.00', '1120000.00'),
(49, 29383, 71, 28, '17052000.00', '40000.00', '17092000.00'),
(50, 29383, 18, 29, '19227000.00', '40000.00', '19267000.00'),
(51, 29383, 42, 30, '3630000.00', '40000.00', '3670000.00'),
(52, 30341, 46, 42, '224532000.00', '40000.00', '224572000.00'),
(53, 30341, 4, 41, '6642000.00', '40000.00', '6682000.00'),
(54, 30341, 50, 30, '3240000.00', '40000.00', '3280000.00'),
(55, 31938, 33, 13, '19175000.00', '40000.00', '19215000.00'),
(56, 31938, 42, 47, '5687000.00', '40000.00', '5727000.00'),
(57, 31938, 33, 13, '19175000.00', '40000.00', '19215000.00'),
(58, 35046, 63, 21, '2835000.00', '40000.00', '2875000.00'),
(59, 35046, 30, 13, '45565000.00', '40000.00', '45605000.00'),
(60, 35046, 58, 45, '78570000.00', '40000.00', '78610000.00'),
(61, 39876, 23, 43, '232200000.00', '40000.00', '232240000.00'),
(62, 39876, 48, 46, '14306000.00', '40000.00', '14346000.00'),
(63, 39876, 9, 41, '183106000.00', '40000.00', '183146000.00'),
(64, 40132, 31, 26, '41860000.00', '40000.00', '41900000.00'),
(65, 40132, 9, 36, '160776000.00', '40000.00', '160816000.00'),
(66, 40132, 5, 16, '2592000.00', '40000.00', '2632000.00'),
(67, 43399, 44, 35, '14210000.00', '40000.00', '14250000.00'),
(68, 43399, 13, 2, '54000.00', '40000.00', '94000.00'),
(69, 43399, 61, 8, '1728000.00', '40000.00', '1768000.00'),
(70, 44231, 46, 11, '58806000.00', '40000.00', '58846000.00'),
(71, 44231, 8, 43, '14534000.00', '40000.00', '14574000.00'),
(72, 46147, 27, 37, '12987000.00', '40000.00', '13027000.00'),
(73, 46147, 63, 39, '5265000.00', '40000.00', '5305000.00'),
(74, 46147, 16, 12, '134640000.00', '40000.00', '134680000.00'),
(75, 46597, 56, 47, '22231000.00', '40000.00', '22271000.00'),
(76, 46597, 26, 10, '4190000.00', '40000.00', '4230000.00'),
(77, 46597, 32, 1, '7552000.00', '40000.00', '7592000.00'),
(78, 47877, 24, 16, '1072000.00', '40000.00', '1112000.00'),
(79, 47877, 13, 19, '513000.00', '40000.00', '553000.00'),
(80, 47877, 37, 49, '197617000.00', '40000.00', '197657000.00'),
(81, 48167, 32, 43, '324736000.00', '40000.00', '324776000.00'),
(82, 48167, 70, 45, '21285000.00', '40000.00', '21325000.00'),
(83, 48167, 51, 27, '219267000.00', '40000.00', '219307000.00'),
(84, 49927, 14, 26, '702000.00', '40000.00', '742000.00'),
(85, 49927, 42, 28, '3388000.00', '40000.00', '3428000.00'),
(86, 49927, 55, 39, '1053000.00', '40000.00', '1093000.00'),
(87, 50854, 14, 27, '729000.00', '40000.00', '769000.00'),
(88, 50854, 35, 12, '10068000.00', '40000.00', '10108000.00'),
(89, 50854, 25, 42, '2268000.00', '40000.00', '2308000.00'),
(90, 52867, 9, 15, '66990000.00', '40000.00', '67030000.00'),
(91, 52999, 42, 5, '605000.00', '40000.00', '645000.00'),
(92, 52999, 12, 39, '6318000.00', '40000.00', '6358000.00'),
(93, 52999, 67, 7, '1036000.00', '40000.00', '1076000.00'),
(94, 55554, 66, 6, '486000.00', '40000.00', '526000.00'),
(95, 55554, 45, 49, '23177000.00', '40000.00', '23217000.00'),
(96, 55554, 25, 9, '486000.00', '40000.00', '526000.00'),
(97, 56224, 2, 35, '5670000.00', '40000.00', '5710000.00'),
(98, 56224, 10, 32, '62368000.00', '40000.00', '62408000.00'),
(99, 56224, 54, 46, '6210000.00', '40000.00', '6250000.00'),
(100, 56224, 9, 9, '40194000.00', '40000.00', '40234000.00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `weight` decimal(8,2) DEFAULT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `stock`, `discount`, `weight`, `photo`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'Accessory28', '1069000.00', 10, 15, '2000.00', '1.jpg', 'The black-metallic KX-TGF38M system makes communication more versatile for home and home office. Link2Cell syncs up to two smartphones to make and take cell calls from handsets, and a downloadable Android App alerts you when your cell receives email, updates from social media and more.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(2, 'Acco Pressboard Covers with Storage Hooks, 14 7/8" x 11", Dark Blue', '162000.00', 10, 18, '14000.00', '2.jpg', 'Top and bottom loading binder expandable for various sized projects. Retractable storage hooks for single point or drop file hanging systems. Adjustable flexible nylon posts allow maximum capacity with minimum storage space. Embossed acrylic-coated pressboard cover resists moisture and scuff marks. Designed for use with sheets that are still attached in continuous form with accordion fold. Capacity Range Max: 6amp;quot; Colors: Dark Blue Binder Sheet Size: 11 x 14 7/8 Binder Style: Non-View.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(3, 'Adams Phone Message Book, Professional, 400 Message Capacity, 5 3/6" x 11"', '81000.00', 10, 13, '14000.00', '3.jpg', 'You''ll get the message with this convenient, message book from Adams! The Adams Phone Message Book is the perfect solution for any small business or service provider. Each book contains a total of 400 sets, with four message forms per page, and all sheets are made with 60 percent recycled and 30 percent post-consumer recycled materials. Adams Phone Message Books features a 2-part carbonless format with a white and canary paper sequence to provide an extra copy of all notes written. Each book measures 5.25 x 11 inches (size of the page with 4 messages on it) and features a secure, flexible spiral binding which allows the book to lay flat when open for easy note-taking. This package contains 1 individual booklet, for a total of 400 message sets. Adams Business Forms provide the tools to help keep track of messages, finances, transactions, employees, taxes, and customers to businesses throughout the world. Whether it''s a multi-part form, notebooks, writing pads, record books, or any of the hundreds of items we offer, you can count on Adams products to help!', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(4, 'Avery 51', '162000.00', 10, 10, '20000.00', '4.jpg', 'Hunting for missing documents can be a real time waster. But you can speed up the process by keeping on top of filing with perforated suspension tabs and suspension files, making sure that all your files are labelled correctly. Taking a few moments now to create your tabs in advance is a great way to save you and your colleagues’ time. These suspension tabs are for suspension files and can be personalised with the file contents. Whatever you are filing you can create printed tabs so they are easy for everyone to read. It really will impress your colleagues.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(5, 'Avery Flip-Chart Easel Binder, Black', '162000.00', 10, 17, '12000.00', '5.jpg', 'Ideal for proposals and manuals Cover folds back to form a landscape format easel for presentations. Includes eight three-hole punched, clear sheet protectors. Holds unpunched sheets up to 11 x 8-1/2. 1" capacity.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(6, 'Avery Poly Binder Pockets', '40000.00', 10, 17, '17000.00', '6.jpg', 'Storage becomes simple when you''ve got the right help. These Binder Pockets let you store upto 25 pages of standard 8-1/2 x 11 Inches paper in a three-ring binder without punching or folding. Durable material protects your pages and resists tearing on the rings even with repeated use. The pockets are acid free and archival safe and won''t lift print off pages, so you can store papers for long periods. Pack includes one clear, pink, green, blue and yellow. Maintain your documents with ease and free your mind for care-free joy.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(7, 'Barrel Sharpener', '135000.00', 10, 19, '21000.00', '7.jpg', 'The ultimate accoutrement to your UD pencil collection, this premium-quality dual sharpener will keep all your pencils sharp. Designed for use with our ENTIRE pencil lineup, Grindhouse includes two sharpeners. Use the slimline sharpener with our 24/7 Glide-On Eye and Lip Pencils and the larger sharpener with our thick-barreled 24/7 Concealer Pencils, 24/7 Shadow Pencils and Super-Saturated pencils. Each blade is made of durable steel infused with 1% carbon to prevent rust. ', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(8, 'Belkin 107-key enhanced keyboard, USB/PS/2 interface', '338000.00', 10, 7, '25000.00', '8.jpg', 'The BELKIN ErgoBoard is a truly unique offering with a 3-D, split-design keyboard engineered to ensure that your shoulders, arms, wrists, hands, and fingers can be positioned naturally. Even the longest reports or games won''t give you a pain. For enhanced support and protection from stress, the keyboard has an integrated wrist rest, allowing you to relax your hands in the middle of long typing sessions.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(9, 'Bevis 36 x 72 Conference Tables', '4466000.00', 10, 15, '26000.00', '9.jpg', 'Oval conference table is part of the Loral 87000 Series Wood Laminate Furniture. Table features a slab base design and high-pressure, 1-1/4" thick laminate top.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(10, 'Bevis Steel Folding Chairs', '1949000.00', 10, 10, '21000.00', '10.jpg', 'Bevis Steel Folding Chairs', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(11, 'Binney & Smith inkTank™ Erasable Desk Highlighter, Chisel Tip, Yellow, 12/Box', '121000.00', 10, 6, '16000.00', '11.jpg', 'Boldly emphasize and underline in brilliant, smooth color with Sharpie Liquid Highlighters. Loaded with fluorescent yellow highlighter ink that’s both highly visible and effortlessly smooth, it’s easy to draw attention to just the right passages. The highlighter pen also has a fine chisel that glides across the page for clean marks. And the visible ink supply ensures you’re never left high and dry.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(12, 'Black Print Carbonless Snap-Off® Rapid Letter, 8 1/2" x 7"', '162000.00', 10, 12, '22000.00', '12.jpg', 'Stay compliant with the laws when shipping dangerous goods using the TOPS Hazardous Material Short Form. It allows you to itemize up to 10 articles and it includes a memorandum copy for the agent''s or shipper''s files. Each form has a hazardous material check-off column that is highlighted with a red symbol. It also offers a space for an emergency response phone number. The TOPS form measures 8.5" x 7" and each package comes with 50 forms.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(13, 'BoxOffice By Design Rectangular and Half-Moon Meeting Room Tables', '17000000.00', 10, 9, '12000.00', '13.jpg', 'This square table set is comprised of six 72"W Flipper nesting tables. Set up is a snap. Simply roll the nesting tables into place, and flip up the tops. A grommet is included in each table for wire management; a trough in the modesty panel keeps wires neat and out of the way. Break down is just as easy. The top surfaces drop down with the touch of a lever, and the tables nest for space-saving storage.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(14, 'Cardinal Holdit Business Card Pockets', '27000.00', 10, 11, '26000.00', '14.jpg', 'Business Card Pockets,Top Load,3-3/4"x2-3/8",10/PK,Clear', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(15, 'Cardinal Poly Pocket Divider Pockets for Ring Binders', '81000.00', 10, 18, '5000.00', '15.jpg', 'TOPS Cardinal Poly Ring Binder Pockets are manufactured with super strong poly pockets that outperform regular paper pockets for binders. This extra tough material will not fray or tear. These products are made from archival-quality polypropylene, which is scratch-resistant and transfer-safe so it will not damage photos or transfer inks or toners. Poly Ring Binder Pockets are available in white with a pocket on both the front and back of each divider. Each pack includes one set of five pocket pages, with a 20 sheet capacity on each pocket for a total of 40 sheets per divider. You can count on TOPS high quality products for trusted, professional indexing solutions for your workplace and workday!', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(16, 'Chromcraft Bull-Nose Wood 48" x 96" Rectangular Conference Tables', '11220000.00', 10, 20, '6000.00', '16.jpg', 'Wood Bullnose Rectangular Conference Table Top, Base? Manufacturer?s five-year warrantyStain-, scratch- and burn-resistant solid hardwood. 2" radiused (bullnose) edge. Top and Base sold and shipped separately?ORDER BOTH. Meets or exceeds ANSI/BIFMA standards. Assembly required. (Cannot ship UPS.)Top Component96w x 48d x 30h (with base). Total shpg. wt. 262 lbs. with base.Oak Finish', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(17, 'Companion Letter/Legal File, Black', '636000.00', 10, 13, '9000.00', '17.jpg', 'Large capacity file has easy lift handle and transparent lid. Holds letter- or legal-size folders. 40 lb capacity. Fits into the Heavy Duty File Shuttle (FS-2BHD, sold separately) for easy mobility. Black.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(18, 'DPC 650 Piper', '663000.00', 10, 19, '7000.00', '18.jpg', 'This cellphone product is manufactured by or for MOTOROLA, by a mobile phone accessory manufacturer and distributor of good quality cell phone accessories. ', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(19, 'Eldon ClusterMat Chair Mat with Cordless Antistatic Protection', '1339000.00', 10, 13, '19000.00', '19.jpg', 'Constant movement around electrical devices such as computers, photocopiers and medical equipment, causes a build-up of static and dust, damaging the machines and shortening their life span. Made by Floortex, Computex Anti-Static mats provide total static protection and attract dust away from sensitive areas, creating a safer environment for the equipment and the people who use it. It''s gripper back will hold the mat in place on carpet. Floortex''s unique PVC mats are the clearest vinyl mats on the market, offering higher quality, less odor and higher UV protection than other vinyl mats.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(20, 'Eldon Pizzaz™ Desk Accessories', '514000.00', 10, 18, '14000.00', '20.jpg', 'Divided compartments make it easy to organize pens, pencils, markers, scissors, rulers and more, keeping them uncluttered and within reach. Two additional smaller compartments are great for paper clips and other small accessories. Sleek hidden drawer adds additional storage the perfect size for 3 x 3 sticky note pads. Sturdy, stylish black mesh metal goes with any office decor. Features Material(s): Steel Number of compartments: 8 Color(s): Black', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(21, 'Fellowes Basic 104-Key Keyboard, Platinum', '433000.00', 10, 8, '21000.00', '21.jpg', 'Compact, lightweight, this replacement keyboard offers quality and durability at the right price.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(22, 'Fellowes Binding Cases', '3600000.00', 10, 18, '26000.00', '22.jpg', 'Fellowes Star Plus Binding Machine', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(23, 'Fellowes PB300 Plastic Comb Binding Machine', '5400000.00', 10, 8, '12000.00', '23.jpg', 'The Fellowes Comb Pulsar+ 300 Binding Machine is a manual comb binder which is designed for regular use in small to medium sized offices, or in the home. It features vertical sheet loading and an adjustable edge guide to ensure consistent punch alignment. It also comes with a starter kit so you can begin using this machine right away.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(24, 'Fiskars 8" Scissors, 2/Pack', '67000.00', 10, 8, '10000.00', '24.jpg', 'The Fiskars Recycled Everyday scissors is suitable for cutting a variety of office materials like paper, boxes and envelopes. Our stainless steel blades provide smooth-cutting action and the lightweight design gives you more cutting control for everyday tasks.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(25, 'Fiskars® Softgrip Scissors', '54000.00', 10, 9, '12000.00', '25.jpg', 'Performance Softgrip series straight scissors. Durable hardened stainless steel blades. Thicker blades for lasting durability. Softgrip ergonomic handles for added comfort and control. Handles contain 30% post consumer recycled plastic. Limited lifetime warranty.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(26, 'GBC Binding covers', '419000.00', 10, 16, '20000.00', '26.jpg', 'These GBC Binding Covers can be used to give your bound documents a professional finish. They''re designed to enhance the appearance of your reports and can help to protect your pages against wear and tear.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(27, 'GBC Standard Therm-A-Bind Covers', '351000.00', 10, 5, '9000.00', '27.jpg', 'Designed for the GBC ThermaBind system, Standard Thermal Binding Covers give a stylish ‘perfect bound’ look to documents. They combine a transparent PVC front to display the title page clearly and a white silk-finish back, forming a totally secure spine with permanently bound pages, and are available in a wide range of sizes to bind up to 440 sheets.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(28, 'GE 4 Foot Flourescent Tube, 40 Watt', '297000.00', 10, 9, '1000.00', '28.jpg', 'GE, F40/BLB, 40 W, 48", Black Light Bulb, Medium Bi Pin, 20,000 Hours.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(29, 'Global Adaptabilities™ Conference Tables', '16499000.00', 10, 15, '17000.00', '29.jpg', 'Global Conference Tables Are Made To Order And Vary By Color ', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(30, 'Global Commerce™ Series High-Back Swivel/Tilt Chairs', '3505000.00', 10, 9, '13000.00', '30.jpg', 'Contemporary design provides ergonomic comfort and support. Synthetic air-flow mesh fabric with mock leather trim. Injection molded base and frame. Pneumatic seat height adjustment from 16-1/2" to 20-1/2". Seat depth adjustment plus tilt tension and tilt control. Height-adjustable lock. 360° swivel.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(31, 'Global Deluxe Stacking Chair, Gray', '1610000.00', 10, 16, '17000.00', '31.jpg', 'Make your hosting duties easier with durable, comfortable chairs that clean up in a breeze. In antique linen with a powder-coated finish for durability. The vinyl padded seat and back make them a crowd favorite. This folding chair offers a tube-in-tube reinforced frame and low maintenance, long lasting powder coat frame finish, while still remaining stylish with fashionable vinyl upholstery that matches any current decor. Chairs have a strong, two cross brace construction and a comfortable, contoured padded seat and back.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(32, 'Global High-Back Leather Tilter, Burgundy', '7552000.00', 10, 17, '28000.00', '32.jpg', 'Contemporary office furniture from Global will have you on the fast track to success. With sturdy construction and attractive finish., this comfortable yet powerful design is an inviting addition to any workplace environment, sure to blend with your decor. Features: Open arms are fully padded and upholstered in Leather Seat height adjustment and infinite tilt lock/seat tilt control are standard Executive Headrest provides an additional 7 inches of head and neck support for executive applications', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(33, 'Global Leather Task Chair, Black', '1475000.00', 10, 11, '16000.00', '33.jpg', 'Ergonomic executive chair upholstered in bonded black leather and PVC Padded seat and back for all-day comfort and support Pneumatic seat-height adjustment', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(34, 'Hewlett-Packard Business Color Inkjet 3000 [N, DTN] Series Printers', '4046000.00', 10, 5, '8000.00', '34.jpg', 'BUSINESS INKJET 3000-21/18PPM DUAL PROC 88MB', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(35, 'Hewlett-Packard cp1700 [D, PS] Series Color Inkjet Printers', '839000.00', 10, 13, '27000.00', '35.jpg', 'Professional-quality wide format printing, up to 13-by-19-inch posters, 13-by-50-inch banners Up to 2,400 x 1,200 dpi color resolution on premium photo papers Up to 16 ppm black, 14.5 ppm color Connect via USB, parallel, or infrared ports; optional networking PC and Mac compatible', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(36, 'Holmes Replacement Filter for HEPA Air Cleaner, Medium Room', '1610000.00', 10, 15, '5000.00', '36.jpg', 'Manufacturer: Holmes Products. Sold Individually Harmony® 99.97% HEPA Air Purifier Quiet operation, slim design and 20% more performance! 99.97% HEPA filtration/carbon odor filter clears air of most airborne bacteria and molds, pet dander and tobacco', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(37, 'Hon 2111 Invitation™ Series Corner Table', '4033000.00', 10, 19, '10000.00', '37.jpg', 'Vertical filing cabinet features steel ball-bearing suspension for superior suspension, 28-1/2 deep files, label holders and One Key core-removable lock kit. High drawer sides accept hanging folders without the use of hangrails. All drawers have thumb latches.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(38, 'Hon 4700 Series Mobuis™ Mid-Back Task Chairs with Adjustable Arms', '7525000.00', 10, 10, '24000.00', '38.jpg', 'Comfortable ergonomic seating. Curved back provides dynamic lumbar support as it gently “hugs” the torso. Super-strong polymer shell flexes in response to your every movement. Upholstery has stain-resistant protection. “Inverse-synchro tilt” opens torso for better circulation.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(39, 'Ibico Recycled Linen-Style Covers', '148000.00', 10, 20, '1000.00', '39.jpg', 'As the name suggests, LinenWeave™ Covers evoke the elegant, natural fibre look and feel of linen. Made from a high content wood pulp and recycled materials, these sturdy A4, 250gsm covers incorporate colour fastening features to ensure your reports stay looking pristine and don’t fade over time. With a classic colour palette to match, LinenWeave™ Covers give a top quality, professional finish to presentations and reports.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(40, 'Imation 3.5 IBM Formatted Diskettes, 10/Box', '54000.00', 10, 18, '24000.00', '40.jpg', 'Cost effective method of backup, storage, transporting and sharing data. Enhanced low torque reduces diskette drive wear. Antistatic design protects against static buildup.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(41, 'Imation Primaris 3.5" 2HD Unformatted Diskettes, 10/Pack', '324000.00', 10, 14, '10000.00', '41.jpg', 'Imation neon floppy disks. Package of ten.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(42, 'Master Giant Foot® Doorstop, Safety Yellow', '121000.00', 10, 11, '21000.00', '42.jpg', 'Giant Foot(R) doorstops will not crush, slide underneath doors or leave marks on floors or carpeting. They are designed to hold extra-large, heavy doors with clearances up to two inches from the floor and doors with spring closures. The ultimate doorstop for commercial, industrial or other heavy door usage.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(43, 'Microsoft Natural Multimedia Keyboard', '460000.00', 10, 8, '30000.00', '43.jpg', 'Take command of your keyboard experience in comfort and style! Discover the Microsoft® Natural MultiMedia Keyboard, with one-touch buttons that take you directly to your favorite multimedia activities – navigate music and video clips, surf the Web, and start many of the programs you use most. With a stylish ergonomic design, this keyboard adds dynamic features to your desktop.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(44, 'Newell 323', '406000.00', 10, 7, '30000.00', '44.jpg', 'Newell 323', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(45, 'Newell 340', '473000.00', 10, 14, '15000.00', '45.jpg', 'Newell 340', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(46, 'Office Star - Mid Back Dual function Ergonomic High Back Chair with 2-Way Adjustable Arms', '5346000.00', 10, 8, '13000.00', '46.jpg', 'Progrid back managers chair with 2-way adjustable arms, dual function control and seat slider.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(47, 'Peel & Stick Add-On Corner Pockets', '148000.00', 10, 7, '27000.00', '47.jpg', 'Holds 8 1/2" x 11" papers. Papers slip in and out easily, making this pocket perfect for the inside front and back binder covers. Clear adhesive-backed pockets identify, protect & organize. Items can be inserted & changed quickly and easily. This pack contains 10 pockets.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(48, 'Poly Designer Cover & Back', '311000.00', 10, 17, '15000.00', '48.jpg', 'For over 60 years the GBC brand has been a world leader in products that help consumers protect, preserve, secure, organize and enhance their printed materials. GBC Design View Poly Presentation Covers are contemporary designer covers in heavy duty frosted with an embossed design.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(49, 'Project Tote Personal File', '406000.00', 10, 9, '16000.00', '49.jpg', 'For business or personal files, our Weathertight Portable File Tote is an exceptional solution! Not only will it accommodate a generous number of letter-size hanging file folders (sold separately), it also includes an organizer compartment to keep office supplies handy.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(50, 'Rediform S.O.S. Phone Message Books', '108000.00', 10, 20, '12000.00', '50.jpg', 'Law record book. Sewn binding with hard burgundy leather-like cover. Blue and red ruling. 150 sheets. Numbered pages. Center line. Recycled paper with a minimum of 30% post-consumer content.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(51, 'SAFCO PlanMaster Heigh-Adjustable Drafting Table Base, 43w x 30d x 30-37h, Black', '8121000.00', 10, 6, '19000.00', '51.jpg', 'Achieve creative savings. At a fraction of the cost, the PlanMaster Table Base provides the height and board angle adjustment of a traditional 4-post table. Raise your drafting table with a convenient spring-assisted mechanism that adjusts from 30" to 37" in height. You don''t have to scrimp on quality to save.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(52, 'Sauder Forest Hills Library, Woodland Oak Finish', '2016000.00', 10, 17, '25000.00', '52.jpg', 'For a stately look in bookcases or to display your. The Brookstone Collection, in caramel birch; The Mission Collection, of fruitwood; Sauder Mission Collection. We have 2027 products for Sauder Forest Hills Collection like.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(53, 'Self-Adhesive Ring Binder Labels', '54000.00', 10, 7, '5000.00', '53.jpg', 'Keep your shelves organized and make binders easier to identify with these self-adhesive labels. Conveniently packed so an office of any size can stay organized. Includes cardstock inserts for the label holder. Hand write or use a label maker for quick customization of each insert. Or, print on your own 8.5 x 11 paper using free templates from C-Line free downloadable insert template. For 2 inch binders. 12/PK.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(54, 'Staples Vinyl Coated Paper Clips, 800/Box', '135000.00', 10, 14, '4000.00', '54.jpg', 'Staples Jumbo paper clip in assorted colors prevents snags or marks on paper. Smooth clip with vinyl-coating is easy to grasp and sold as 500 per tub. Keep papers in your office easily organized using these Staples jumbo vinyl-coated paper clips. An assortment of colors in each tub of 500 makes it simple to color-code materials, so you always know which documents belong with each project. Bulk Packaging Each tub of Staples jumbo vinyl-coated paper clips contains 500 paper clips, ensuring you always have them on hand for needed projects. Each color features its own compartment, so you won''t have to dig to find a paper clip compatible with your chosen project as you can use the different colors to keep essential projects organized.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(55, 'Stockwell Push Pins', '27000.00', 10, 17, '3000.00', '55.jpg', 'T-Pins with 9/16 wide heads are a great solution for posting items to cork, fabric, wood and other surfaces.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(56, 'T28 WORLD', '473000.00', 10, 14, '27000.00', '56.jpg', 'Ericsson believes in an "all communicating" world. Voice, data, images and video are conveniently communicated anywhere and anytime in the world, increasing both quality-of-life, productivity and enabling a more resource-efficient world. Ericsson is one of the major progressive forces, active around the globe, driving for this advanced communication to happen.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(57, 'Tenex 46" x 60" Computer Anti-Static Chairmat, Rectangular Shaped', '460000.00', 10, 15, '3000.00', '57.jpg', 'The Cleartex AdvantageMat 36" x 48" Chair Mat offers reliable floor protection both at office and home. Use this vinyl chairmat in heavy foot traffic, spillage risk, or chair movement areas, like receptions, waste bin locations, desk regions, and more to shield the surface from hard wear and tear.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(58, 'Tenex Contemporary Contur Chairmats for Low and Medium Pile Carpet, Computer, 39" x 49"', '1746000.00', 10, 13, '10000.00', '58.jpg', 'Floortex Ultimat is the brand name for transparent, clear floor protection mats in Original Floortex Polycarbonate. Ultimat mats will allow the beauty of your floors to shine through, and will protect your carpeted areas from wear and tear. Suitable for low to medium pile carpets up to 1/2" thick. Flooring can be expensive - in those areas where there is heavy foot traffic, chair movement or risk of spillage, a relatively small investment in a high quality Ultimats will help protect your carpet from damage, reduce cleaning costs and increasing their lifespan. Ultimats are versatile, easy to move and clean, making them perfect for a wide variety of uses.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(59, 'Tenex File Box, Personal Filing Tote with Lid, Black', '324000.00', 10, 11, '26000.00', '59.jpg', 'Box portable carrying handle for transport handle folds flush when not in use designed for letter sized hanging file folders heavy duty buckle latch.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(60, 'Tennsco Snap-Together Open Shelving Units, Starter Sets and Add-On Units', '4967000.00', 10, 6, '27000.00', '60.jpg', 'Save 50% of the floor space used by conventional files. For highly active files and records. Provides a single freestanding unit or use with Add-On Units for a complete wall system. Units snap together without tools or fasteners. All shelves are adjustable. Order one Starter Set and any number of matching height Add-On Units. Two ''''L'''' uprights for the frame. Includes adjustable 12'''' deep shelves, shelf supports and five file dividers per shelf. 200- to 300-lb. load capacity per shelf with weight evenly distributed. Heavy-duty rolled steel construction with sand finish.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(61, 'White GlueTop Scratch Pads', '216000.00', 10, 8, '21000.00', '61.jpg', 'Unruled white recycled paper pad contains at least 50 percent recycled wastepaper with 10 percent post-consumer waste fiber.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(62, 'Wilson Jones DublLock® D-Ring Binders', '243000.00', 10, 7, '24000.00', '62.jpg', 'Wilson Jones DublLock D-Ring Binder', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(63, 'Wilson Jones Hanging View Binder, White, 1"', '135000.00', 10, 11, '23000.00', '63.jpg', 'Hanging round ring view binder features retractable hooks and locking round rings for maximum ease of use. Offers 175 sheet capacity in 1 inch binder size. Hangs in most standard file cabinet drawers or file frames.', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(64, 'Wirebound Message Book, 4 per Page', '148000.00', 10, 6, '24000.00', '64.jpg', 'National Brand Visitor''s Register Book, Black, 8.5 x 9.875 Inches, 64 Sheets . ', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(65, 'Xerox 1906', '798000.00', 10, 12, '2000.00', '65.jpg', 'Vitality 100% Recycled Multipurpose Printer Paper, 8-1/2 x 11, White 5000 Sheets', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(66, 'Xerox 1939', '81000.00', 10, 12, '4000.00', '66.jpg', 'Letter Multipurpose Pastel Paper, Yellow', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(67, 'Xerox 1978', '148000.00', 10, 17, '1000.00', '67.jpg', 'Letter Gloss Digital Elite Laser Paper', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(68, 'Xerox 212', '297000.00', 10, 9, '5000.00', '68.jpg', 'Gloss Digital Elite Laser Paper', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(69, 'Xerox 217', '67000.00', 10, 14, '4000.00', '69.jpg', 'Pastel multipurpose paper is versatile enough for most jobs Great for color coding and organizing', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(70, 'Xerox 224', '473000.00', 10, 6, '3000.00', '70.jpg', 'Multipurpose printer paper is versatile and good for most jobs Features ColorLok Technology for bolder blacks and vivid colors with inkjet printing Perfect for giving documents a crisp', '2016-12-07 08:22:45', '2016-12-07 08:22:45'),
(71, 'Xerox 227', '609000.00', 10, 7, '3000.00', '71.jpg', 'Vitality Multipurpose Printer Paper, 8 1/2 x 14, White, 5,000 Sheets/CT', '2016-12-07 08:22:45', '2016-12-07 08:22:45');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `created_at`, `updated_at`) VALUES
('1', 'Bali', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('10', 'Jawa Tengah', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('11', 'Jawa Timur', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('12', 'Kalimantan Barat', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('13', 'Kalimantan Selatan', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('14', 'Kalimantan Tengah', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('15', 'Kalimantan Timur', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('16', 'Kalimantan Utara', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('17', 'Kepulauan Riau', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('18', 'Lampung', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('19', 'Maluku', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('2', 'Bangka Belitung', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('20', 'Maluku Utara', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('21', 'Nanggroe Aceh Darussalam (NAD)', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('22', 'Nusa Tenggara Barat (NTB)', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('23', 'Nusa Tenggara Timur (NTT)', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('24', 'Papua', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('25', 'Papua Barat', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('26', 'Riau', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('27', 'Sulawesi Barat', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('28', 'Sulawesi Selatan', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('29', 'Sulawesi Tengah', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('3', 'Banten', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('30', 'Sulawesi Tenggara', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('31', 'Sulawesi Utara', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('32', 'Sumatera Barat', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('33', 'Sumatera Selatan', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('34', 'Sumatera Utara', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('4', 'Bengkulu', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('5', 'DI Yogyakarta', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('6', 'DKI Jakarta', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('7', 'Gorontalo', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('8', 'Jambi', '2016-04-24 11:39:52', '2016-04-24 11:39:52'),
('9', 'Jawa Barat', '2016-04-24 11:39:52', '2016-04-24 11:39:52');

-- --------------------------------------------------------

--
-- Table structure for table `recommendations`
--

CREATE TABLE `recommendations` (
  `id` int(10) NOT NULL,
  `product_id` int(4) NOT NULL,
  `recommendation_id` int(4) NOT NULL,
  `support` int(3) NOT NULL,
  `confidence` int(3) NOT NULL,
  `user_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recommendations`
--

INSERT INTO `recommendations` (`id`, `product_id`, `recommendation_id`, `support`, `confidence`, `user_id`) VALUES
(1, 1, 9, 3, 100, 10),
(2, 1, 28, 3, 100, 11),
(3, 28, 1, 3, 50, 21),
(4, 1, 57, 3, 100, 11),
(5, 57, 1, 3, 100, 21),
(6, 2, 9, 3, 100, 10),
(7, 2, 10, 3, 100, 18),
(8, 10, 2, 3, 100, 28),
(9, 2, 54, 3, 100, 23),
(10, 54, 2, 3, 100, 30),
(11, 3, 22, 3, 100, 18),
(12, 22, 3, 3, 100, 28),
(13, 3, 42, 3, 100, 24),
(14, 4, 46, 3, 100, 3),
(15, 46, 4, 3, 33, 3),
(16, 4, 50, 3, 100, 8),
(17, 50, 4, 3, 100, 28),
(18, 5, 9, 3, 100, 22),
(19, 5, 31, 3, 100, 26),
(20, 31, 5, 3, 100, 29),
(21, 6, 9, 3, 100, 8),
(22, 6, 20, 3, 100, 11),
(23, 20, 6, 3, 100, 30),
(24, 7, 9, 3, 100, 24),
(25, 7, 17, 3, 100, 29),
(26, 17, 7, 3, 100, 11),
(27, 8, 46, 3, 100, 28),
(28, 46, 8, 3, 33, 13),
(29, 10, 9, 3, 100, 13),
(30, 17, 9, 3, 100, 24),
(31, 20, 9, 3, 100, 16),
(32, 23, 9, 3, 100, 8),
(33, 28, 9, 3, 50, 22),
(34, 31, 9, 3, 100, 22),
(35, 48, 9, 3, 100, 14),
(36, 54, 9, 3, 100, 2),
(37, 57, 9, 3, 100, 27),
(38, 10, 54, 3, 100, 6),
(39, 54, 10, 3, 100, 7),
(40, 11, 21, 3, 100, 20),
(41, 21, 11, 3, 50, 14),
(42, 11, 25, 3, 100, 11),
(43, 11, 49, 3, 100, 9),
(44, 49, 11, 3, 100, 11),
(45, 11, 68, 3, 100, 28),
(46, 68, 11, 3, 100, 12),
(47, 12, 42, 3, 100, 9),
(48, 12, 67, 3, 100, 5),
(49, 67, 12, 3, 100, 28),
(50, 13, 19, 3, 33, 32),
(51, 19, 13, 3, 100, 12),
(52, 13, 24, 3, 33, 27),
(53, 24, 13, 3, 100, 5),
(54, 13, 37, 3, 33, 5),
(55, 37, 13, 3, 100, 10),
(56, 13, 44, 3, 33, 2),
(57, 44, 13, 3, 100, 10),
(58, 13, 52, 3, 33, 12),
(59, 52, 13, 3, 100, 28),
(60, 13, 61, 3, 33, 11),
(61, 61, 13, 3, 100, 1),
(62, 14, 25, 3, 50, 4),
(63, 14, 35, 3, 50, 13),
(64, 35, 14, 3, 100, 23),
(65, 14, 42, 3, 50, 13),
(66, 14, 55, 3, 50, 27),
(67, 55, 14, 3, 100, 3),
(68, 15, 28, 3, 100, 23),
(69, 28, 15, 3, 50, 13),
(70, 15, 32, 3, 100, 26),
(71, 15, 36, 3, 100, 30),
(72, 36, 15, 3, 100, 7),
(73, 16, 27, 3, 100, 5),
(74, 27, 16, 3, 50, 3),
(75, 16, 63, 3, 100, 1),
(76, 63, 16, 3, 33, 27),
(77, 18, 40, 3, 100, 5),
(78, 40, 18, 3, 100, 6),
(79, 18, 42, 3, 100, 12),
(80, 19, 52, 3, 100, 12),
(81, 52, 19, 3, 100, 24),
(82, 21, 25, 3, 50, 21),
(83, 21, 39, 3, 50, 1),
(84, 39, 21, 3, 100, 3),
(85, 21, 49, 3, 50, 10),
(86, 49, 21, 3, 100, 9),
(87, 21, 53, 3, 50, 13),
(88, 53, 21, 3, 100, 9),
(89, 21, 59, 3, 50, 2),
(90, 59, 21, 3, 100, 16),
(91, 21, 62, 3, 50, 9),
(92, 62, 21, 3, 100, 26),
(93, 21, 63, 3, 50, 10),
(94, 63, 21, 3, 33, 4),
(95, 21, 68, 3, 50, 17),
(96, 68, 21, 3, 100, 10),
(97, 22, 42, 3, 100, 29),
(98, 23, 48, 3, 100, 22),
(99, 48, 23, 3, 100, 20),
(100, 24, 37, 3, 100, 3),
(101, 37, 24, 3, 100, 17),
(102, 27, 25, 3, 50, 13),
(103, 34, 25, 3, 100, 14),
(104, 35, 25, 3, 100, 30),
(105, 41, 25, 3, 100, 14),
(106, 45, 25, 3, 100, 11),
(107, 47, 25, 3, 100, 11),
(108, 49, 25, 3, 100, 22),
(109, 66, 25, 3, 100, 15),
(110, 68, 25, 3, 100, 6),
(111, 69, 25, 3, 100, 15),
(112, 26, 32, 3, 100, 24),
(113, 26, 56, 3, 100, 12),
(114, 56, 26, 3, 100, 19),
(115, 27, 34, 3, 50, 28),
(116, 34, 27, 3, 100, 20),
(117, 27, 47, 3, 50, 14),
(118, 47, 27, 3, 100, 10),
(119, 27, 63, 3, 50, 6),
(120, 63, 27, 3, 33, 28),
(121, 27, 69, 3, 50, 30),
(122, 69, 27, 3, 100, 5),
(123, 28, 32, 3, 50, 25),
(124, 28, 36, 3, 50, 15),
(125, 36, 28, 3, 100, 2),
(126, 28, 57, 3, 50, 23),
(127, 57, 28, 3, 100, 17),
(128, 29, 38, 3, 100, 13),
(129, 38, 29, 3, 100, 15),
(130, 29, 46, 3, 100, 3),
(131, 46, 29, 3, 33, 30),
(132, 30, 58, 3, 100, 18),
(133, 58, 30, 3, 100, 31),
(134, 30, 63, 3, 100, 4),
(135, 63, 30, 3, 33, 19),
(136, 36, 32, 3, 100, 22),
(137, 43, 32, 3, 100, 21),
(138, 51, 32, 3, 100, 7),
(139, 56, 32, 3, 100, 1),
(140, 65, 32, 3, 100, 14),
(141, 70, 32, 3, 100, 6),
(142, 33, 42, 3, 100, 18),
(143, 34, 47, 3, 100, 10),
(144, 47, 34, 3, 100, 23),
(145, 34, 69, 3, 100, 23),
(146, 69, 34, 3, 100, 16),
(147, 38, 46, 3, 100, 7),
(148, 46, 38, 3, 33, 20),
(149, 39, 53, 3, 100, 13),
(150, 53, 39, 3, 100, 6),
(151, 39, 59, 3, 100, 19),
(152, 59, 39, 3, 100, 17),
(153, 39, 62, 3, 100, 24),
(154, 62, 39, 3, 100, 7),
(155, 39, 63, 3, 100, 24),
(156, 63, 39, 3, 33, 3),
(157, 40, 42, 3, 100, 6),
(158, 55, 42, 3, 100, 21),
(159, 60, 42, 3, 100, 25),
(160, 64, 42, 3, 100, 27),
(161, 67, 42, 3, 100, 28),
(162, 43, 65, 3, 100, 26),
(163, 65, 43, 3, 100, 14),
(164, 44, 61, 3, 100, 23),
(165, 61, 44, 3, 100, 8),
(166, 45, 66, 3, 100, 3),
(167, 66, 45, 3, 100, 21),
(168, 46, 50, 3, 33, 31),
(169, 50, 46, 3, 100, 1),
(170, 47, 69, 3, 100, 3),
(171, 69, 47, 3, 100, 11),
(172, 49, 68, 3, 100, 12),
(173, 68, 49, 3, 100, 28),
(174, 51, 70, 3, 100, 9),
(175, 70, 51, 3, 100, 23),
(176, 53, 59, 3, 100, 25),
(177, 59, 53, 3, 100, 24),
(178, 53, 62, 3, 100, 11),
(179, 62, 53, 3, 100, 15),
(180, 53, 63, 3, 100, 8),
(181, 63, 53, 3, 33, 25),
(182, 58, 63, 3, 100, 10),
(183, 63, 58, 3, 33, 2),
(184, 59, 62, 3, 100, 9),
(185, 62, 59, 3, 100, 10),
(186, 59, 63, 3, 100, 21),
(187, 63, 59, 3, 33, 11),
(188, 60, 64, 3, 100, 25),
(189, 64, 60, 3, 100, 26),
(190, 62, 63, 3, 100, 22),
(191, 63, 62, 3, 33, 3);

-- --------------------------------------------------------

--
-- Table structure for table `regencies`
--

CREATE TABLE `regencies` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `province_id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `regencies`
--

INSERT INTO `regencies` (`id`, `province_id`, `name`, `created_at`, `updated_at`) VALUES
('1', '21', 'Kabupaten Aceh Barat', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('10', '21', 'Kabupaten Aceh Timur', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('100', '19', 'Kabupaten Buru Selatan', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('101', '30', 'Kabupaten Buton', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('102', '30', 'Kabupaten Buton Utara', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('103', '9', 'Kabupaten Ciamis', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('104', '9', 'Kabupaten Cianjur', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('105', '10', 'Kabupaten Cilacap', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('106', '3', 'Kota Cilegon', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('107', '9', 'Kota Cimahi', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('108', '9', 'Kabupaten Cirebon', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('109', '9', 'Kota Cirebon', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('11', '21', 'Kabupaten Aceh Utara', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('110', '34', 'Kabupaten Dairi', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('111', '24', 'Kabupaten Deiyai (Deliyai)', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('112', '34', 'Kabupaten Deli Serdang', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('113', '10', 'Kabupaten Demak', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('114', '1', 'Kota Denpasar', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('115', '9', 'Kota Depok', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('116', '32', 'Kabupaten Dharmasraya', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('117', '24', 'Kabupaten Dogiyai', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('118', '22', 'Kabupaten Dompu', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('119', '29', 'Kabupaten Donggala', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('12', '32', 'Kabupaten Agam', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('120', '26', 'Kota Dumai', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('121', '33', 'Kabupaten Empat Lawang', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('122', '23', 'Kabupaten Ende', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('123', '28', 'Kabupaten Enrekang', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('124', '25', 'Kabupaten Fakfak', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('125', '23', 'Kabupaten Flores Timur', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('126', '9', 'Kabupaten Garut', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('127', '21', 'Kabupaten Gayo Lues', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('128', '1', 'Kabupaten Gianyar', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('129', '7', 'Kabupaten Gorontalo', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('13', '23', 'Kabupaten Alor', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('130', '7', 'Kota Gorontalo', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('131', '7', 'Kabupaten Gorontalo Utara', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('132', '28', 'Kabupaten Gowa', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('133', '11', 'Kabupaten Gresik', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('134', '10', 'Kabupaten Grobogan', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('135', '5', 'Kabupaten Gunung Kidul', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('136', '14', 'Kabupaten Gunung Mas', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('137', '34', 'Kota Gunungsitoli', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('138', '20', 'Kabupaten Halmahera Barat', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('139', '20', 'Kabupaten Halmahera Selatan', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('14', '19', 'Kota Ambon', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('140', '20', 'Kabupaten Halmahera Tengah', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('141', '20', 'Kabupaten Halmahera Timur', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('142', '20', 'Kabupaten Halmahera Utara', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('143', '13', 'Kabupaten Hulu Sungai Selatan', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('144', '13', 'Kabupaten Hulu Sungai Tengah', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('145', '13', 'Kabupaten Hulu Sungai Utara', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('146', '34', 'Kabupaten Humbang Hasundutan', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('147', '26', 'Kabupaten Indragiri Hilir', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('148', '26', 'Kabupaten Indragiri Hulu', '2016-04-24 11:39:57', '2016-04-24 11:39:57'),
('149', '9', 'Kabupaten Indramayu', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('15', '34', 'Kabupaten Asahan', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('150', '24', 'Kabupaten Intan Jaya', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('151', '6', 'Kota Jakarta Barat', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('152', '6', 'Kota Jakarta Pusat', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('153', '6', 'Kota Jakarta Selatan', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('154', '6', 'Kota Jakarta Timur', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('155', '6', 'Kota Jakarta Utara', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('156', '8', 'Kota Jambi', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('157', '24', 'Kabupaten Jayapura', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('158', '24', 'Kota Jayapura', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('159', '24', 'Kabupaten Jayawijaya', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('16', '24', 'Kabupaten Asmat', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('160', '11', 'Kabupaten Jember', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('161', '1', 'Kabupaten Jembrana', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('162', '28', 'Kabupaten Jeneponto', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('163', '10', 'Kabupaten Jepara', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('164', '11', 'Kabupaten Jombang', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('165', '25', 'Kabupaten Kaimana', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('166', '26', 'Kabupaten Kampar', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('167', '14', 'Kabupaten Kapuas', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('168', '12', 'Kabupaten Kapuas Hulu', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('169', '10', 'Kabupaten Karanganyar', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('17', '1', 'Kabupaten Badung', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('170', '1', 'Kabupaten Karangasem', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('171', '9', 'Kabupaten Karawang', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('172', '17', 'Kabupaten Karimun', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('173', '34', 'Kabupaten Karo', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('174', '14', 'Kabupaten Katingan', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('175', '4', 'Kabupaten Kaur', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('176', '12', 'Kabupaten Kayong Utara', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('177', '10', 'Kabupaten Kebumen', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('178', '11', 'Kabupaten Kediri', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('179', '11', 'Kota Kediri', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('18', '13', 'Kabupaten Balangan', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('180', '24', 'Kabupaten Keerom', '2016-04-24 11:39:58', '2016-04-24 11:39:58'),
('181', '10', 'Kabupaten Kendal', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('182', '30', 'Kota Kendari', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('183', '4', 'Kabupaten Kepahiang', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('184', '17', 'Kabupaten Kepulauan Anambas', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('185', '19', 'Kabupaten Kepulauan Aru', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('186', '32', 'Kabupaten Kepulauan Mentawai', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('187', '26', 'Kabupaten Kepulauan Meranti', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('188', '31', 'Kabupaten Kepulauan Sangihe', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('189', '6', 'Kabupaten Kepulauan Seribu', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('19', '15', 'Kota Balikpapan', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('190', '31', 'Kabupaten Kepulauan Siau Tagulandang Biaro (Sitaro)', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('191', '20', 'Kabupaten Kepulauan Sula', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('192', '31', 'Kabupaten Kepulauan Talaud', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('193', '24', 'Kabupaten Kepulauan Yapen (Yapen Waropen)', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('194', '8', 'Kabupaten Kerinci', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('195', '12', 'Kabupaten Ketapang', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('196', '10', 'Kabupaten Klaten', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('197', '1', 'Kabupaten Klungkung', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('198', '30', 'Kabupaten Kolaka', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('199', '30', 'Kabupaten Kolaka Utara', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('2', '21', 'Kabupaten Aceh Barat Daya', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('20', '21', 'Kota Banda Aceh', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('200', '30', 'Kabupaten Konawe', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('201', '30', 'Kabupaten Konawe Selatan', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('202', '30', 'Kabupaten Konawe Utara', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('203', '13', 'Kabupaten Kotabaru', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('204', '31', 'Kota Kotamobagu', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('205', '14', 'Kabupaten Kotawaringin Barat', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('206', '14', 'Kabupaten Kotawaringin Timur', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('207', '26', 'Kabupaten Kuantan Singingi', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('208', '12', 'Kabupaten Kubu Raya', '2016-04-24 11:39:59', '2016-04-24 11:39:59'),
('209', '10', 'Kabupaten Kudus', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('21', '18', 'Kota Bandar Lampung', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('210', '5', 'Kabupaten Kulon Progo', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('211', '9', 'Kabupaten Kuningan', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('212', '23', 'Kabupaten Kupang', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('213', '23', 'Kota Kupang', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('214', '15', 'Kabupaten Kutai Barat', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('215', '15', 'Kabupaten Kutai Kartanegara', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('216', '15', 'Kabupaten Kutai Timur', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('217', '34', 'Kabupaten Labuhan Batu', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('218', '34', 'Kabupaten Labuhan Batu Selatan', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('219', '34', 'Kabupaten Labuhan Batu Utara', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('22', '9', 'Kabupaten Bandung', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('220', '33', 'Kabupaten Lahat', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('221', '14', 'Kabupaten Lamandau', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('222', '11', 'Kabupaten Lamongan', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('223', '18', 'Kabupaten Lampung Barat', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('224', '18', 'Kabupaten Lampung Selatan', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('225', '18', 'Kabupaten Lampung Tengah', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('226', '18', 'Kabupaten Lampung Timur', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('227', '18', 'Kabupaten Lampung Utara', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('228', '12', 'Kabupaten Landak', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('229', '34', 'Kabupaten Langkat', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('23', '9', 'Kota Bandung', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('230', '21', 'Kota Langsa', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('231', '24', 'Kabupaten Lanny Jaya', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('232', '3', 'Kabupaten Lebak', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('233', '4', 'Kabupaten Lebong', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('234', '23', 'Kabupaten Lembata', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('235', '21', 'Kota Lhokseumawe', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('236', '32', 'Kabupaten Lima Puluh Koto/Kota', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('237', '17', 'Kabupaten Lingga', '2016-04-24 11:40:00', '2016-04-24 11:40:00'),
('238', '22', 'Kabupaten Lombok Barat', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('239', '22', 'Kabupaten Lombok Tengah', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('24', '9', 'Kabupaten Bandung Barat', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('240', '22', 'Kabupaten Lombok Timur', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('241', '22', 'Kabupaten Lombok Utara', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('242', '33', 'Kota Lubuk Linggau', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('243', '11', 'Kabupaten Lumajang', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('244', '28', 'Kabupaten Luwu', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('245', '28', 'Kabupaten Luwu Timur', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('246', '28', 'Kabupaten Luwu Utara', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('247', '11', 'Kabupaten Madiun', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('248', '11', 'Kota Madiun', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('249', '10', 'Kabupaten Magelang', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('25', '29', 'Kabupaten Banggai', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('250', '10', 'Kota Magelang', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('251', '11', 'Kabupaten Magetan', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('252', '9', 'Kabupaten Majalengka', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('253', '27', 'Kabupaten Majene', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('254', '28', 'Kota Makassar', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('255', '11', 'Kabupaten Malang', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('256', '11', 'Kota Malang', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('257', '16', 'Kabupaten Malinau', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('258', '19', 'Kabupaten Maluku Barat Daya', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('259', '19', 'Kabupaten Maluku Tengah', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('26', '29', 'Kabupaten Banggai Kepulauan', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('260', '19', 'Kabupaten Maluku Tenggara', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('261', '19', 'Kabupaten Maluku Tenggara Barat', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('262', '27', 'Kabupaten Mamasa', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('263', '24', 'Kabupaten Mamberamo Raya', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('264', '24', 'Kabupaten Mamberamo Tengah', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('265', '27', 'Kabupaten Mamuju', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('266', '27', 'Kabupaten Mamuju Utara', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('267', '31', 'Kota Manado', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('268', '34', 'Kabupaten Mandailing Natal', '2016-04-24 11:40:01', '2016-04-24 11:40:01'),
('269', '23', 'Kabupaten Manggarai', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('27', '2', 'Kabupaten Bangka', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('270', '23', 'Kabupaten Manggarai Barat', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('271', '23', 'Kabupaten Manggarai Timur', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('272', '25', 'Kabupaten Manokwari', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('273', '25', 'Kabupaten Manokwari Selatan', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('274', '24', 'Kabupaten Mappi', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('275', '28', 'Kabupaten Maros', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('276', '22', 'Kota Mataram', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('277', '25', 'Kabupaten Maybrat', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('278', '34', 'Kota Medan', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('279', '12', 'Kabupaten Melawi', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('28', '2', 'Kabupaten Bangka Barat', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('280', '8', 'Kabupaten Merangin', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('281', '24', 'Kabupaten Merauke', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('282', '18', 'Kabupaten Mesuji', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('283', '18', 'Kota Metro', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('284', '24', 'Kabupaten Mimika', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('285', '31', 'Kabupaten Minahasa', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('286', '31', 'Kabupaten Minahasa Selatan', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('287', '31', 'Kabupaten Minahasa Tenggara', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('288', '31', 'Kabupaten Minahasa Utara', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('289', '11', 'Kabupaten Mojokerto', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('29', '2', 'Kabupaten Bangka Selatan', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('290', '11', 'Kota Mojokerto', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('291', '29', 'Kabupaten Morowali', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('292', '33', 'Kabupaten Muara Enim', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('293', '8', 'Kabupaten Muaro Jambi', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('294', '4', 'Kabupaten Muko Muko', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('295', '30', 'Kabupaten Muna', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('296', '14', 'Kabupaten Murung Raya', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('297', '33', 'Kabupaten Musi Banyuasin', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('298', '33', 'Kabupaten Musi Rawas', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('299', '24', 'Kabupaten Nabire', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('3', '21', 'Kabupaten Aceh Besar', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('30', '2', 'Kabupaten Bangka Tengah', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('300', '21', 'Kabupaten Nagan Raya', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('301', '23', 'Kabupaten Nagekeo', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('302', '17', 'Kabupaten Natuna', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('303', '24', 'Kabupaten Nduga', '2016-04-24 11:40:02', '2016-04-24 11:40:02'),
('304', '23', 'Kabupaten Ngada', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('305', '11', 'Kabupaten Nganjuk', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('306', '11', 'Kabupaten Ngawi', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('307', '34', 'Kabupaten Nias', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('308', '34', 'Kabupaten Nias Barat', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('309', '34', 'Kabupaten Nias Selatan', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('31', '11', 'Kabupaten Bangkalan', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('310', '34', 'Kabupaten Nias Utara', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('311', '16', 'Kabupaten Nunukan', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('312', '33', 'Kabupaten Ogan Ilir', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('313', '33', 'Kabupaten Ogan Komering Ilir', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('314', '33', 'Kabupaten Ogan Komering Ulu', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('315', '33', 'Kabupaten Ogan Komering Ulu Selatan', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('316', '33', 'Kabupaten Ogan Komering Ulu Timur', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('317', '11', 'Kabupaten Pacitan', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('318', '32', 'Kota Padang', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('319', '34', 'Kabupaten Padang Lawas', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('32', '1', 'Kabupaten Bangli', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('320', '34', 'Kabupaten Padang Lawas Utara', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('321', '32', 'Kota Padang Panjang', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('322', '32', 'Kabupaten Padang Pariaman', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('323', '34', 'Kota Padang Sidempuan', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('324', '33', 'Kota Pagar Alam', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('325', '34', 'Kabupaten Pakpak Bharat', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('326', '14', 'Kota Palangka Raya', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('327', '33', 'Kota Palembang', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('328', '28', 'Kota Palopo', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('329', '29', 'Kota Palu', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('33', '13', 'Kabupaten Banjar', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('330', '11', 'Kabupaten Pamekasan', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('331', '3', 'Kabupaten Pandeglang', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('332', '9', 'Kabupaten Pangandaran', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('333', '28', 'Kabupaten Pangkajene Kepulauan', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('334', '2', 'Kota Pangkal Pinang', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('335', '24', 'Kabupaten Paniai', '2016-04-24 11:40:03', '2016-04-24 11:40:03'),
('336', '28', 'Kota Parepare', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('337', '32', 'Kota Pariaman', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('338', '29', 'Kabupaten Parigi Moutong', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('339', '32', 'Kabupaten Pasaman', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('34', '9', 'Kota Banjar', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('340', '32', 'Kabupaten Pasaman Barat', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('341', '15', 'Kabupaten Paser', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('342', '11', 'Kabupaten Pasuruan', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('343', '11', 'Kota Pasuruan', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('344', '10', 'Kabupaten Pati', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('345', '32', 'Kota Payakumbuh', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('346', '25', 'Kabupaten Pegunungan Arfak', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('347', '24', 'Kabupaten Pegunungan Bintang', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('348', '10', 'Kabupaten Pekalongan', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('349', '10', 'Kota Pekalongan', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('35', '13', 'Kota Banjarbaru', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('350', '26', 'Kota Pekanbaru', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('351', '26', 'Kabupaten Pelalawan', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('352', '10', 'Kabupaten Pemalang', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('353', '34', 'Kota Pematang Siantar', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('354', '15', 'Kabupaten Penajam Paser Utara', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('355', '18', 'Kabupaten Pesawaran', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('356', '18', 'Kabupaten Pesisir Barat', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('357', '32', 'Kabupaten Pesisir Selatan', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('358', '21', 'Kabupaten Pidie', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('359', '21', 'Kabupaten Pidie Jaya', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('36', '13', 'Kota Banjarmasin', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('360', '28', 'Kabupaten Pinrang', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('361', '7', 'Kabupaten Pohuwato', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('362', '27', 'Kabupaten Polewali Mandar', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('363', '11', 'Kabupaten Ponorogo', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('364', '12', 'Kabupaten Pontianak', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('365', '12', 'Kota Pontianak', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('366', '29', 'Kabupaten Poso', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('367', '33', 'Kota Prabumulih', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('368', '18', 'Kabupaten Pringsewu', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('369', '11', 'Kabupaten Probolinggo', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('37', '10', 'Kabupaten Banjarnegara', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('370', '11', 'Kota Probolinggo', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('371', '14', 'Kabupaten Pulang Pisau', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('372', '20', 'Kabupaten Pulau Morotai', '2016-04-24 11:40:04', '2016-04-24 11:40:04'),
('373', '24', 'Kabupaten Puncak', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('374', '24', 'Kabupaten Puncak Jaya', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('375', '10', 'Kabupaten Purbalingga', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('376', '9', 'Kabupaten Purwakarta', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('377', '10', 'Kabupaten Purworejo', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('378', '25', 'Kabupaten Raja Ampat', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('379', '4', 'Kabupaten Rejang Lebong', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('38', '28', 'Kabupaten Bantaeng', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('380', '10', 'Kabupaten Rembang', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('381', '26', 'Kabupaten Rokan Hilir', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('382', '26', 'Kabupaten Rokan Hulu', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('383', '23', 'Kabupaten Rote Ndao', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('384', '21', 'Kota Sabang', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('385', '23', 'Kabupaten Sabu Raijua', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('386', '10', 'Kota Salatiga', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('387', '15', 'Kota Samarinda', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('388', '12', 'Kabupaten Sambas', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('389', '34', 'Kabupaten Samosir', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('39', '5', 'Kabupaten Bantul', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('390', '11', 'Kabupaten Sampang', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('391', '12', 'Kabupaten Sanggau', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('392', '24', 'Kabupaten Sarmi', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('393', '8', 'Kabupaten Sarolangun', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('394', '32', 'Kota Sawah Lunto', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('395', '12', 'Kabupaten Sekadau', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('396', '28', 'Kabupaten Selayar (Kepulauan Selayar)', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('397', '4', 'Kabupaten Seluma', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('398', '10', 'Kabupaten Semarang', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('399', '10', 'Kota Semarang', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('4', '21', 'Kabupaten Aceh Jaya', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('40', '33', 'Kabupaten Banyuasin', '2016-04-24 11:39:54', '2016-04-24 11:39:54'),
('400', '19', 'Kabupaten Seram Bagian Barat', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('401', '19', 'Kabupaten Seram Bagian Timur', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('402', '3', 'Kabupaten Serang', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('403', '3', 'Kota Serang', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('404', '34', 'Kabupaten Serdang Bedagai', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('405', '14', 'Kabupaten Seruyan', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('406', '26', 'Kabupaten Siak', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('407', '34', 'Kota Sibolga', '2016-04-24 11:40:05', '2016-04-24 11:40:05'),
('408', '28', 'Kabupaten Sidenreng Rappang/Rapang', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('409', '11', 'Kabupaten Sidoarjo', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('41', '10', 'Kabupaten Banyumas', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('410', '29', 'Kabupaten Sigi', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('411', '32', 'Kabupaten Sijunjung (Sawah Lunto Sijunjung)', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('412', '23', 'Kabupaten Sikka', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('413', '34', 'Kabupaten Simalungun', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('414', '21', 'Kabupaten Simeulue', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('415', '12', 'Kota Singkawang', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('416', '28', 'Kabupaten Sinjai', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('417', '12', 'Kabupaten Sintang', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('418', '11', 'Kabupaten Situbondo', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('419', '5', 'Kabupaten Sleman', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('42', '11', 'Kabupaten Banyuwangi', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('420', '32', 'Kabupaten Solok', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('421', '32', 'Kota Solok', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('422', '32', 'Kabupaten Solok Selatan', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('423', '28', 'Kabupaten Soppeng', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('424', '25', 'Kabupaten Sorong', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('425', '25', 'Kota Sorong', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('426', '25', 'Kabupaten Sorong Selatan', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('427', '10', 'Kabupaten Sragen', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('428', '9', 'Kabupaten Subang', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('429', '21', 'Kota Subulussalam', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('43', '13', 'Kabupaten Barito Kuala', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('430', '9', 'Kabupaten Sukabumi', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('431', '9', 'Kota Sukabumi', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('432', '14', 'Kabupaten Sukamara', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('433', '10', 'Kabupaten Sukoharjo', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('434', '23', 'Kabupaten Sumba Barat', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('435', '23', 'Kabupaten Sumba Barat Daya', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('436', '23', 'Kabupaten Sumba Tengah', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('437', '23', 'Kabupaten Sumba Timur', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('438', '22', 'Kabupaten Sumbawa', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('439', '22', 'Kabupaten Sumbawa Barat', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('44', '14', 'Kabupaten Barito Selatan', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('440', '9', 'Kabupaten Sumedang', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('441', '11', 'Kabupaten Sumenep', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('442', '8', 'Kota Sungaipenuh', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('443', '24', 'Kabupaten Supiori', '2016-04-24 11:40:06', '2016-04-24 11:40:06'),
('444', '11', 'Kota Surabaya', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('445', '10', 'Kota Surakarta (Solo)', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('446', '13', 'Kabupaten Tabalong', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('447', '1', 'Kabupaten Tabanan', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('448', '28', 'Kabupaten Takalar', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('449', '25', 'Kabupaten Tambrauw', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('45', '14', 'Kabupaten Barito Timur', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('450', '16', 'Kabupaten Tana Tidung', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('451', '28', 'Kabupaten Tana Toraja', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('452', '13', 'Kabupaten Tanah Bumbu', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('453', '32', 'Kabupaten Tanah Datar', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('454', '13', 'Kabupaten Tanah Laut', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('455', '3', 'Kabupaten Tangerang', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('456', '3', 'Kota Tangerang', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('457', '3', 'Kota Tangerang Selatan', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('458', '18', 'Kabupaten Tanggamus', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('459', '34', 'Kota Tanjung Balai', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('46', '14', 'Kabupaten Barito Utara', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('460', '8', 'Kabupaten Tanjung Jabung Barat', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('461', '8', 'Kabupaten Tanjung Jabung Timur', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('462', '17', 'Kota Tanjung Pinang', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('463', '34', 'Kabupaten Tapanuli Selatan', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('464', '34', 'Kabupaten Tapanuli Tengah', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('465', '34', 'Kabupaten Tapanuli Utara', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('466', '13', 'Kabupaten Tapin', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('467', '16', 'Kota Tarakan', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('468', '9', 'Kabupaten Tasikmalaya', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('469', '9', 'Kota Tasikmalaya', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('47', '28', 'Kabupaten Barru', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('470', '34', 'Kota Tebing Tinggi', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('471', '8', 'Kabupaten Tebo', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('472', '10', 'Kabupaten Tegal', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('473', '10', 'Kota Tegal', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('474', '25', 'Kabupaten Teluk Bintuni', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('475', '25', 'Kabupaten Teluk Wondama', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('476', '10', 'Kabupaten Temanggung', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('477', '20', 'Kota Ternate', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('478', '20', 'Kota Tidore Kepulauan', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('479', '23', 'Kabupaten Timor Tengah Selatan', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('48', '17', 'Kota Batam', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('480', '23', 'Kabupaten Timor Tengah Utara', '2016-04-24 11:40:07', '2016-04-24 11:40:07'),
('481', '34', 'Kabupaten Toba Samosir', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('482', '29', 'Kabupaten Tojo Una-Una', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('483', '29', 'Kabupaten Toli-Toli', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('484', '24', 'Kabupaten Tolikara', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('485', '31', 'Kota Tomohon', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('486', '28', 'Kabupaten Toraja Utara', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('487', '11', 'Kabupaten Trenggalek', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('488', '19', 'Kota Tual', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('489', '11', 'Kabupaten Tuban', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('49', '10', 'Kabupaten Batang', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('490', '18', 'Kabupaten Tulang Bawang', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('491', '18', 'Kabupaten Tulang Bawang Barat', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('492', '11', 'Kabupaten Tulungagung', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('493', '28', 'Kabupaten Wajo', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('494', '30', 'Kabupaten Wakatobi', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('495', '24', 'Kabupaten Waropen', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('496', '18', 'Kabupaten Way Kanan', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('497', '10', 'Kabupaten Wonogiri', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('498', '10', 'Kabupaten Wonosobo', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('499', '24', 'Kabupaten Yahukimo', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('5', '21', 'Kabupaten Aceh Selatan', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('50', '8', 'Kabupaten Batang Hari', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('500', '24', 'Kabupaten Yalimo', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('501', '5', 'Kota Yogyakarta', '2016-04-24 11:40:08', '2016-04-24 11:40:08'),
('51', '11', 'Kota Batu', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('52', '34', 'Kabupaten Batu Bara', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('53', '30', 'Kota Bau-Bau', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('54', '9', 'Kabupaten Bekasi', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('55', '9', 'Kota Bekasi', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('56', '2', 'Kabupaten Belitung', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('57', '2', 'Kabupaten Belitung Timur', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('58', '23', 'Kabupaten Belu', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('59', '21', 'Kabupaten Bener Meriah', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('6', '21', 'Kabupaten Aceh Singkil', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('60', '26', 'Kabupaten Bengkalis', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('61', '12', 'Kabupaten Bengkayang', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('62', '4', 'Kota Bengkulu', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('63', '4', 'Kabupaten Bengkulu Selatan', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('64', '4', 'Kabupaten Bengkulu Tengah', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('65', '4', 'Kabupaten Bengkulu Utara', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('66', '15', 'Kabupaten Berau', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('67', '24', 'Kabupaten Biak Numfor', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('68', '22', 'Kabupaten Bima', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('69', '22', 'Kota Bima', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('7', '21', 'Kabupaten Aceh Tamiang', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('70', '34', 'Kota Binjai', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('71', '17', 'Kabupaten Bintan', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('72', '21', 'Kabupaten Bireuen', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('73', '31', 'Kota Bitung', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('74', '11', 'Kabupaten Blitar', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('75', '11', 'Kota Blitar', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('76', '10', 'Kabupaten Blora', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('77', '7', 'Kabupaten Boalemo', '2016-04-24 11:39:55', '2016-04-24 11:39:55'),
('78', '9', 'Kabupaten Bogor', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('79', '9', 'Kota Bogor', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('8', '21', 'Kabupaten Aceh Tengah', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('80', '11', 'Kabupaten Bojonegoro', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('81', '31', 'Kabupaten Bolaang Mongondow (Bolmong)', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('82', '31', 'Kabupaten Bolaang Mongondow Selatan', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('83', '31', 'Kabupaten Bolaang Mongondow Timur', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('84', '31', 'Kabupaten Bolaang Mongondow Utara', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('85', '30', 'Kabupaten Bombana', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('86', '11', 'Kabupaten Bondowoso', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('87', '28', 'Kabupaten Bone', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('88', '7', 'Kabupaten Bone Bolango', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('89', '15', 'Kota Bontang', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('9', '21', 'Kabupaten Aceh Tenggara', '2016-04-24 11:39:53', '2016-04-24 11:39:53'),
('90', '24', 'Kabupaten Boven Digoel', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('91', '10', 'Kabupaten Boyolali', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('92', '10', 'Kabupaten Brebes', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('93', '32', 'Kota Bukittinggi', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('94', '1', 'Kabupaten Buleleng', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('95', '28', 'Kabupaten Bulukumba', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('96', '16', 'Kabupaten Bulungan (Bulongan)', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('97', '8', 'Kabupaten Bungo', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('98', '29', 'Kabupaten Buol', '2016-04-24 11:39:56', '2016-04-24 11:39:56'),
('99', '19', 'Kabupaten Buru', '2016-04-24 11:39:56', '2016-04-24 11:39:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@feloria.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'admin', 'cTIpAzzURJWEnolBeNj3Z8DtAmdHECyW8N8LweoUtIAmQ15YqLVw57b2EiuU', '2016-12-01 03:28:15', '2017-04-03 10:34:28'),
(2, 'Alejandro Ballentine', 'ballentine@gmail.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:16', '2016-12-01 03:28:16'),
(3, 'Alex Avila', 'avila@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'eNRKCzeZPOJw5fzjO5NRk1QAe0wM3ETjSdZTDyWhXND8YHtNZd0MWa6GPG1P', '2016-12-01 03:28:17', '2017-01-25 03:53:15'),
(4, 'Alyssa Tate', 'tate@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:18', '2016-12-01 03:28:18'),
(5, 'Bill Donatelli', 'donatelli@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:19', '2016-12-01 03:28:19'),
(6, 'Cari MacIntyre', 'macintyre@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:20', '2016-12-01 03:28:20'),
(7, 'Cari Sayre', 'sayre@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:21', '2016-12-01 03:28:21'),
(8, 'Christopher Conant', 'conant@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'S9yAqKbmCpBj3va0WrYyGt60iDYiyEBzVDLs0xvLen2abeXfGKWJLUngNULn', '2016-12-01 03:28:22', '2017-01-25 04:21:33'),
(9, 'Dan Lawera', 'lawera@gmail.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'YNYuo4nC5jDXK0V7HxM0qfWmu1HhUgKYTqVfmHQ39W9JcqW8TiYDrwzmoqDP', '2016-12-01 03:28:23', '2017-01-25 04:22:54'),
(10, 'Dana Kaydos', 'kaydos@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '8OGQ0toan4syPdxlV8C9R6m6hN7bOgAIN90rqn7JbzYVGHOES05PB3l3fC45', '2016-12-01 03:28:24', '2017-01-25 04:20:27'),
(11, 'Eugene Hildebrand', 'hildebrand@icloud.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:25', '2016-12-01 03:28:25'),
(12, 'Hunter Glantz', 'glantz@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'wDk4EkC9ezC8ShIUwrZhZ7vCs8VE7fzLqje9AJ0MRUbvpPtBUome44aoea6I', '2016-12-01 03:28:26', '2017-01-25 04:19:28'),
(13, 'Jas O''Carroll', 'o''carroll@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:27', '2016-12-01 03:28:27'),
(14, 'Jill Stevenson', 'stevenson@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'bLEbfAzZALlh4WLNO0Y30OmUXDvTBwpcehjNZx5hiucGOVxeK0uckZOXJWI6', '2016-12-01 03:28:28', '2017-01-25 04:18:35'),
(15, 'John Lee', 'lee@icloud.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:29', '2016-12-01 03:28:29'),
(16, 'Justin Knight', 'knight@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'BuwZWniMH4jponiJSHp88R8wYA1JPPE2Zyj2PWqGUkJMmBMVgzTkVyzDSxhX', '2016-12-01 03:28:30', '2017-04-03 10:33:06'),
(17, 'Khloe Miller', 'miller@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:31', '2016-12-01 03:28:31'),
(18, 'Maria Bertelson', 'bertelson@gmail.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:32', '2016-12-01 03:28:32'),
(19, 'Matt Abelman', 'abelman@icloud.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'xyyfsYrWN3GRyBQ9xKcpO13Opwj8nLB85glwyiE22GsoLIS8KvA9q3T4oD8B', '2016-12-01 03:28:33', '2017-01-25 04:17:42'),
(20, 'Michael Chen', 'chen@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:34', '2016-12-01 03:28:34'),
(21, 'Michelle Arnett', 'arnett@gmail.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:35', '2016-12-01 03:28:35'),
(22, 'Mick Crebagga', 'crebagga@icloud.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:36', '2016-12-01 03:28:36'),
(23, 'Olvera Toch', 'toch@gmail.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:37', '2016-12-01 03:28:37'),
(24, 'Parhena Norris', 'norris@icloud.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:38', '2016-12-01 03:28:38'),
(25, 'Randy Ferguson', 'ferguson@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:39', '2016-12-01 03:28:39'),
(26, 'Ricardo Block', 'block@icloud.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'pgSgdWw9vvzExbgXBrvW2XIUSaQpgVqG9cMMdowmwY8sReXaFx8DX0yJLfNN', '2016-12-01 03:28:40', '2017-01-25 04:16:42'),
(27, 'Rick Wilson', 'wilson@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'vGsfr6j4hDy3whHHhcIN3M7nMFC763s23AG6orgrnqiZW1wOgV0CF8KuHnNF', '2016-12-01 03:28:41', '2017-04-03 08:22:35'),
(28, 'Shahid Shariari', 'shariari@outlook.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:42', '2016-12-01 03:28:42'),
(29, 'Sonia Sunley', 'sunley@yahoo.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:43', '2016-12-01 03:28:43'),
(30, 'Stuart Van', 'van@yahoo.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:44', '2016-12-01 03:28:44'),
(31, 'Tamara Willingham', 'willingham@gmail.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', '', '2016-12-01 03:28:45', '2016-12-01 03:28:45'),
(32, 'Jack O''Briant', 'briant@live.com', '$2y$10$4Iie1rmUcdygqbphryfpwODqwXedkdNbbVEz0XDsQGnaOzsJ17z.C', 'customer', 'DsJP4XEQo0TgmSXUm0FvMcgvHLuHXpJQ0hTt62HJb4AsoIwf3U3ZVu1leMoS', '2016-12-01 03:28:46', '2017-01-25 04:15:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_regency_id_foreign` (`regency_id`),
  ADD KEY `addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recommendations`
--
ALTER TABLE `recommendations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regencies`
--
ALTER TABLE `regencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regencies_province_id_foreign` (`province_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56225;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `recommendations`
--
ALTER TABLE `recommendations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `regencies`
--
ALTER TABLE `regencies`
  ADD CONSTRAINT `regencies_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
