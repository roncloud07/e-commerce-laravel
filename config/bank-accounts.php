<?php
return [
    'bca' => [
        'title' => 'BCA',
        'bank' => 'BCA',
        'name' => 'PT.Feloria',
        'number' => '333 765 8998'
    ],
    'bni' => [
        'title' => 'BNI',
        'bank' => 'BNI',
        'name' => 'PT.Feloria',
        'number' => '465 882 1833'
    ],
    'mandiri' => [
        'title' => 'Mandiri',
        'bank' => 'Mandiri',
        'name' => 'PT.Feloria',
        'number' => '184 384 2904'
    ],
    'atm-bersama' => [
        'title' => 'ATM Bersama',
        'bank' => 'Mandiri',
        'name' => 'PT.Feloria',
        'number' => '273 481 3474'
    ],
];
