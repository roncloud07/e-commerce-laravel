<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            // $table->increments('id');
            // $table->string('name');
            // $table->string('photo');
            // $table->string('model');
            // $table->decimal('price', 10, 2); // max XX,XXX,XXX.XX
            // $table->decimal('weight', 8, 2); // max XXX,XXX,XX
            // $table->timestamps();

            $table->increments('id');
            $table->string('name', 200); 
            // $table->decimal('price', 10, 2); // max XX,XXX,XXX.XX
            // $table->integer('stock'); // integer = column, autoIncrement, unsigned
            // $table->integer('discount')->default(0);
            // $table->decimal('weight', 8, 2); // max XXX,XXX,XX

            $table->decimal('price', 10, 2)->nullable(); // max XX,XXX,XXX.XX
            $table->integer('stock')->nullable(); // integer = column, autoIncrement, unsigned
            $table->integer('discount')->default(0);
            $table->decimal('weight', 8, 2)->nullable(); // max XXX,XXX,XX
            $table->string('photo')->nullable();
            $table->text('detail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
