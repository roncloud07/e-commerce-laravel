<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Review;

class ReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products1 = Product::where('id', '1')->first();
        $products2 = Product::where('id', '2')->first();
        $products3 = Product::where('id', '3')->first();

        $review1 = Review::create([
            'rating'=>'5'
        ]);
        $review2 = Review::create([
            'rating'=>'3'
        ]);
        $review3 = Review::create([
            'rating'=>'2'
        ]);

        $products1->reviews()->save($review1);
		$products2->reviews()->save($review2);
		$products3->reviews()->save($review3);
    }
}
